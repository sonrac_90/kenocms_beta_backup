<?php

session_start();

/**
 * Функции "про запас"
 */
/**
 * Удаление всех переменных сессии
 */
function deleteAllVarSession(){
    foreach($_SESSION as $k=>$v){
        unset($_SESSION[$k]);
        unset($v);
        unset($k);
    }
}

/**
 * Удаление все переменных запросов
 */
function deleteAllVarRequest(){
    foreach($_GET as $k=>$v){
        unset($_GET[$k]);
        unset($v);
        unset($k);
    }
    foreach($_POST as $k=>$v){
        unset($_POST[$k]);
        unset($v);
        unset($k);
    }
    foreach($_REQUEST as $k=>$v){
        unset($_REQUEST[$k]);
        unset($v);
        unset($k);
    }
}

/**
 * Дамп массива $arr
 * @param $arr
 */
function printArr($arr){
    echo '<pre>';
    print_r($arr);
    echo '</pre>';
}

/**
 * Перезаписываем данные сессии
 */
function NewSession(){
    $_SESSION['visit'] = 'comming';
    $_SESSION['day'] = 1;
    $_SESSION['month'] = 1;
    $_SESSION['chCalc'] = 0;
    $_SESSION['baseCalc'] = 0;
    $_SESSION['showResult'] = true;
    $_SESSION['resultTable'] = 'result_not_sort';
}

// Если передан GET параметр truncate, очищаю БД от результатов
if (isset($_GET['truncate'])){
    require_once 'config/config.db.php';
    require_once 'config/config.php';
    require_once 'lang/' . LANG . '/' . CHARSET . '/index.php';
    require_once 'core/error.class.php'; // Класс ошибок
    require_once 'core/database.class.php';
    $db = new KenoCMS_database($langRec);
    $db->TruncateAllTable();
    $db->close();
}

// Если передан GET параметр reset, сбрасываю сессию
if (isset($_GET['reset'])){
    deleteAllVarRequest();
    deleteAllVarSession();
}

// Если сконфигурировано соединение с БД запускаю систему
if (is_file('config/config.db.php')){

    /**
     * Подключаю необходимые модули
     */
    require_once 'core/error.class.php'; // Класс ошибок
    require_once 'core/database.class.php'; // Класс для работы с БД 
    require_once 'config/calc.descr.php'; // Описание расчетов
    require_once 'core/PrintTableClass.php';
    require_once 'core/Calculate/calculate.class.php'; // Класс расчетов
    require_once 'request.php'; // Запросы
    require_once 'lang/' . LANG . '/' . CHARSET . '/index.php'; // Транслит
  
    if (!isset($_SESSION['visit'])){
        NewSession();
    }
    if (isset($_GET['numCalc'])){
        deleteAllVarSession();
        NewSession();
        $_SESSION['baseCalc'] = ($descriptionCalculate[$_GET['numCalc']]['parentCalc'] !== 'not') ?
                                $descriptionCalculate[$_GET['numCalc']]['parentCalc'] : $_GET['numCalc'];
        $_SESSION['chCalc'] = $_GET['numCalc'];
    }

    // Если переходим к расчету
    if (isset($_POST['act']) && ($_POST['act'] === 'goToCalcSet') || !isset($_SESSION['showResult'])){
        if (intval($_SESSION['chCalc']) < 60){
            require_once 'core/Calculate/Numb80Calc.php'; // Класс расчета
            $calculateVar = new KenoCMS_Numb80Retry($langRec, $masPrice, $descriptionCalculate[$_SESSION['chCalc']]);
        }
    }
    if (is_object($calculateVar)){
        // Провожу вычисления
        $calculateVar->calculate(
            $descriptionCalculate[$_SESSION['chCalc']]['typeBuildBaseStr'], // Построение базовой строки 
            $descriptionCalculate[$_SESSION['chCalc']]['typeSquareBuild'], // Квадрата
            $descriptionCalculate[$_SESSION['chCalc']]['typeRotate'], // Тип Вращения
            $descriptionCalculate[$_SESSION['chCalc']]['isAdditions'], // Есть ли приращение
            $descriptionCalculate[$_SESSION['chCalc']]['methodPlus'], // Метод сложения
            $descriptionCalculate[$_SESSION['chCalc']]['typeBuildBaseStrTwo'] // Метод выборки цифр в квадраты (horizontal или vertical)
        );
        
        if (isset($_SESSION['statistics'])){
            $calculateVar->statistics($descriptionCalculate[$_SESSION['chCalc']]['typeSq']);
        }
    }

    // Строю страницу по шаблону
    require_once 'templates/' . TEMPLATES . '/index.php';
}else{ // Не сконфигурировано соединение с БД - запускаю установку
    header('location: Install/index.php');
}

?>
