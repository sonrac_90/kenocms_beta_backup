<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 28.08.13
 * Time: 15:49
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class KenoCMS_Error
 * Вывешивет баннеры в случае ошибки
 */
class KenoCMS_Error {
    private $lang = null;

    /**
     * @param $lang - Инициализация транслита
     */
    public function __construct($lang){
        $this->lang = $lang;
    }

    /**
     * Выводим ошибку 
     * @param $textErr - Текст ошибки
     * @param $typeErr - Тип ошибки
     */
    public function printError($textErr, $typeErr){
        switch ($typeErr){
            case 'db' : // Ошибка возникла при работе с БД
                echo '<div class=opacity></div><div class=error><div class=errTextCont>';
                echo '<div class=titleErrDb>' . $this->lang['connect'] . '</div><br/>';
                echo '<div class=texterr>' . $textErr . '</div>';
                echo '</div></div>';
                break;
            default : 
                break;
        }
    }
}

?>
