<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 30.08.13
 * Time: 17:16
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class KenoCMS_calculate
 * Базовый класс расчетов
 */
class KenoCMS_calculate {

    /**
     * @var array - Информация по позиции
     */
    public $positionInfo = array();

    /**
     * @var array - Количество совпадений для каждой возможной комбинации в статистике
     */
    public $countSq = array();
    
    /**
     * @var int - Количество приращений
     */
    public $maxAddition = 80;

    /**
     * @var int - Количество квадратов
     */
    public $maxRotate = 80;
    
    /**
     * @var string - Сохраняется в формате строки новый вычисленный квадрат
     */
    public $tmpSql = '';
    /**
     * @var int - Предел памяти, выделяемый под расчет (для котроля переменных, в которых хранится сохранение квдаратов)
     */
    public $memoryLimit = 470000;
    
    /**
     * @var array - Сведения о таблицах, в которых происходит сохранение информации
     * squareSave-  результаты расчетов
     * positionPriceSave - выигрышные варианты при выборе позиции
     * positionSquareSave - сохранение квадрата, который содержит выигрышную позицию
     */
    public $tblName = array('squareSave' => '', 'positionPriceSave' => '', 'positionSquareSave' => '');
    
    /**
     * @var KenoCMS_database|null - Ссылка на класс для работы с базой данных
     */
    public $db = null;
    
    /**
     * @var string - Запрос на сохранение квадратов
     */
    public $squareSave = '';

    /**
     * @var string - Запрос на сохранение выигрышных позиций статистики 
     */
    public $positionPriceSave = '';

    /**
     * @var string - Запрос на сохранение выигрышных квадратов
     */
    public $positionSquareSave = '';
    
    /**
     * @var array - Массив выигрыша
     */
    public $masPrice = array();
    
    /**
     * @var array Транслит
     */
    public $lang = array();
    /**
     * @var KenoCMS_PrintTable|null Ссылка на класс, который отрисовывает таблицы результата
     */
    public $pt = null;

    /**
     * @var array Базовая строка
     */
    public $baseArr = array();

    /**
     * @var array Массив поиска
     */
    public $searchArr = array();

    /**
     * @var KenoCMS_Error|null Ссылка на класс, который обрабатывает ошибки
     */
    public $error = null;

    /**
     * @var string Дата тиража в формате ДД.ММ.ГГГГ
     */
    public $date = '';

    /**
     * Информация про расчет
     * @var array
     */
    public $descript = array();

    /**
     * Конструктор
     * @param $lang - транслит
     */
    public function __construct(&$lang, &$ArrPrice=null, $descr=null){
        // Транслит
        $this->lang = $lang;
        // Выигрыши
        $this->masPrice = $ArrPrice;
        // Рисование таблиц
        $this->pt = new KenoCMS_PrintTable($this->lang);
        
        // Вывод ошибок
        $this->error = new KenoCMS_Error($lang);

        // Подключаюсь к Базе данных
        $this->db = new KenoCMS_database($this->lang['db']);

        // Выполняю дополнительные действия класса расчета
        $this->additional();
        
        // Описание расчета
        $this->descript = $descr;
        // Если передан аргумент для очистки всех БД, очищаю их
        if (isset($_GET['truncate'])){
            $this->db->TruncateAllTable();
        }

    }

    /**
     * Вывод таблицы результата
     * @param $day - День
     * @param $month - Месяц
     */
    public function PrintResultTable($day, $month){
        $db = new KenoCMS_database($this->lang['db']);
        $res = $db->GetResultBall($day, $month);
        // Если есть такие рез-ты строим
        if ($db->CountRow($res) > 0){
            $this->pt->PrintResultTable($res);
        }
        $db->close();
    }

    /**
     * Дополнение к конструктору, в зависимости от содержимого класса-наследника
     */
    public function additional (){}

    /**
     * Строим массив двумерный из одномерного
     * @param $countCol - Количество столбцов рез-го массива
     * @param $countRow - Количество строк рез-го массива
     * @param $method - Метод, по которому строится квадрат(0 - строка в строку, 1 - спираль, 2 - диагональ)
     */
    public function buildArrayFromArr($arr, $countCol, $countRow, $method){
        $this->tmpSql = '';
        $resultSq = array();
        if ($method == 0){
            $cnt = 0;
            for ($i = 0; $i < $countRow; $i++){
                for ($j = 0; $j < $countCol; $j++){
                    if (isset($arr[$cnt])){
                        $resultSq[$i][$j] = $arr[$cnt];
                        $this->tmpSql .= ', ' . $resultSq[$i][$j];
                    }
                    $cnt++;
                }
            }
        }else if ($method == 1){
            $resultSq = $this->buildSpiralSq($arr, $countRow, $countCol);
            $this->tmpSql = $this->getSqlFromArr($resultSq);
        }else if ($method == 2){
            $resultSq = $this->buildDiagonalSql($arr, $countRow, $countCol);
            $this->tmpSql = $this->getSqlFromArr($resultSq);
        }

        return $resultSq;
    }

    /**
     * Возвращает строку запроса в базу для сохранения квадрата
     * @param $arr - массив
     * @return string
     */
    public function getSqlFromArr($arr){
        $sql = ''; 
        if (count($arr[0][0]) > 1){
            for ($i = 0; $i < count($arr); $i++){
                if (isset($arr[$i])){
                    $sql .= ', ' . $arr[$i];
                }
            }
        }else{
            for ($i = 0; $i < count($arr); $i++){
                for ($j = 0; $j < count($arr); $j++){
                    if (isset($arr[$i][$j])){
                        $sql .= ', ' . $arr[$i][$j];
                    }
                }
            }
        }
        
        return $sql;
    }
    
    /**
     * Строим квадрат по спирали
     * @param $ish - Исходный массив
     * @param $countRow - Количество строк
     * @param $countCol - количество столбцов
     * @return array - Квадрат
     */
    private function buildSpiralSq($ish, $countRow, $countCol){
        $i = 0;
        $n = $countRow;
        $m = $countCol;
        $countIter = count($ish) - 1;
        $k = 0;
        $arr = array();
        
        while ($i <= $countIter){
            $k++;
            for ($j = $k - 1; $j < $m - $k + 1; $j++)
            {
                $arr[$k - 1][$j] = $ish[$i];
                $i++;
            }

            for ($j = $k; $j < $n - $k + 1; $j++)
            {
                if (intval(($j + 1) * ($m - $k + 1)) != ($n * $m))
                {
                    $arr[$j][$m - $k] = $ish[$i];
                    $i++;
                }
            }

            for ($j = $m - $k - 1; $j >= $k - 1; $j--)
            {
                $arr[$n - $k][$j] = $ish[$i];
                $i++;
            }

            for ($j = $n - $k - 1; $j >= $k; $j--)
            {
                $arr[$j][$k - 1] = $ish[$i];
                $i++;
            }
        }

        $arr[8][8] = null;
        return $arr;
    }

    /**
     * Вычисляю позицию следующей итерации (если итерация закончилась)
     * @param $left - смещение влево (если false, смещаемся вниз)
     * @param $j
     * @param $i
     * @return array (смещение влево, i, j)
     */
    private function NextIter80($left, $j, $i)
    {
        if ($left)
        {
            $i += 1;
            if ($i > 8)
            {
                $j += 1;
                $i = ($i > 8)?(0):$i;
                $j = ($j > 8)?(0):$j;
            }
        }
        else
        {
            $j += 1;
            if ($j > 8)
            {
                $i += 1;
                $i = ($i > 8)?(0):$i;
                $j = ($j > 8)?(0):$j;
            }
        }
        return array('left' => $left, 'i' => $i, 'j' => $j);
    }

    /**
     * Расчет новой итерации
     * @param $left - 
     * @param $j
     * @param $i
     * @return array
     */
    private function calculate_next_iteration($left, $j, $i)
    {
        $cont = false;
        $i += 1;
        if ($i > 8)
        {
            $i = 0;
            $j += 2;
            if ($j > 8)
                $j = 0;
            $cont = true;
            return array('left' => $left, 'i' => $i, 'j' => $j, 'continue' => $cont);
        }

        if ($left)
        {
            $j += 1;
            if ($j > 8)
            {
                $j = 0;
            }
        }
        else
        {
            $j -= 1;
            if ($j < 0)
            {
                $j = 8;
            }
        }

        $left = !($left);
        return array('left' => $left, 'i' => $i, 'j' => $j, 'continue' => $cont);
    }

    /**
     * Построение квадрата по диагонали (реализовано только для квадрата 9х9 с пропуском 81 ячейки)
     * @param $ish - Исходный массив
     * @param $countRow - Количество строк
     * @param $countCol - количество столбцов
     * @param int $interval - Интервал, через который заполняем
     * @return array - Квадрат
     */
    private function buildDiagonalSql($ish, $countRow, $countCol, $interval=1){
        // Интервал между числами
        $dx = $interval;
        // Количество чисел
        $porog = $countCol * $countRow - 1;

        // Текущий индекс исходного массива, который делает новый массив
        $index = 0;

        // Текущая Строка Результирующего массива
        $i = 0;
        // Текущий Столбец Результирующего массива
        $j = 0;
        // Результирующий массив
        $arr = array();
        // Приращение столбца
        $left = 1;
        // Текущая итерация
        $cnt = -1;
        $ind80 = null;

        // Текущий шаг до заполнения
        $dx_now = 0;

        // Вошел ли в 81 ячейку
        $is_80_cell = false;

        // Пока не достигнем порога
        while ($index <= $porog)
        {
            $cnt += 1;
            $dx_now += 1;

            // Если нужно заполнить массив
            if (((($cnt % $dx) == 0) && ($cnt >=$dx)) || ($cnt == 0))
            {
                // Обнуляем значение текущего шага
                $dx_now = 0;

                // Если элемент с индексом [$j][$i] не занят
                if (!isset($arr[$i][$j]))
                {
                    $arr[$i][$j] = $ish[$index];
                }
                else
                {
                    // Если вошли в 81 ячейку (которая должна оставаться пустой)
                    if ($is_80_cell)
                    {
                        // Считаем значение индексов на следующем шаге
                        $data = $this->calculate_next_iteration($left, $j, $i);
                    }
                    else
                    {
                        $data['j'] = $j;
                        $data['i'] = $i;
                        $data['left'] = $left;
                    }
                    // Если уже занята ячейка с индексами $j $i
                    if (isset($arr[$data['i']][$data['j']]))
                    {
                        // Если значение шага - четное, меняем направление приращение $j индекса
                        if (($dx % 2) == 0)
                            $left = !$left;
                        do
                        {
                            // Вычисляю значение следующего шага просмотром соседних свободных ячеек по $j и $i
                            $data = $this->NextIter80($left, $j, $i);
                            $j = $data['j'];
                            $i = $data['i'];
                        }
                        while(isset($arr[$i][$j]));
                    }
                    else
                    {
                        $left = $data['left'];
                        $j = $data['j'];
                        $i = $data['i'];
                    }
                    // Провераю, не попал ли я в 81 ячейку
                    if (($i == 8) && ($j == 8))
                    {
                        // Обнуляем значения индексов, меняем направление, возвращаемся на предыдущую итерацию
                        $cnt--;
                        $j = 0;
                        $i = 0;
                        $left = !$left;
                        continue;
                    }
                    // Заполняю массив
                    $arr[$i][$j] = $ish[$index];
                }
                // Увеличиваю значение текущего элемента исходного массива
                $index+=1;
            }

            if ($index == 80){
                break;
            }

            // Условие, по которому определяю, заполнены ли ячейки в массиве по кругу
            $do_80 = false;
            if (($is_80_cell) && ($index == $ind80))
            {
                // Если шаг = 2, то достаточно лишь проверить, свободна ли следующая ячейка за текущей
                if ($dx == 2)
                {
                    if (isset($arr[$j + 1][$i + 1]))
                        $do_80 = true;
                }
                else
                {
                    // Проверяю, достигнут ли конец интервала
                    if (intval($dx - $dx_now) == 0)
                        $do_80 = true;
                }
            }

            if ($do_80){
                // Вычисляю значение следующей незанятой ячейки
                $data = $this->NextIter80($left, $j, $i);
                $j = $data['j'];
                $i = $data['i'];
            }else{
                $is_80_cell = false;
                // Вычисляю следующий шаг
                $data = $this->calculate_next_iteration($left, $j, $i);
                $left = $data['left'];
                $j = $data['j'];
                $i = $data['i'];
                if ($data['continue'])
                    continue;
            }

            // Проверяю, не попал ли я в 81 ячейку
            if (($i == 8) && ($j == 8))
            {
                $j = 0;
                $i = 0;
                $is_80_cell = true;
                $ind80 = $index;
                continue;
            }
        }
        return $arr;
    }

    /**
     * Возвращение одномерного массива из двумерного
     * @param $arr - Исходный массив
     * @param int $cntCol - Количество столбцов
     * @return array - Результирующий одномерный массив
     */
    public function retOneArrFromTwo($arr, $cntCol=20){
        $resArr = array();
        for ($i = 0; $i < count($arr); $i++){
            for ($j = 0; $j < $cntCol; $j++){
                if (isset($arr[$i][$j])){
                    $resArr[] = $arr[$i][$j];
                }
            }
        }
        
        return $resArr;
    }

    /**
     * "Красивый" Дамп двумерного массива
     * @param $arr - массив
     * @param int $cnt - количество столбцов
     */
    public function PrintArr($arr, $cnt=20, $show=true){
        $str = '<table border=1>' . "\n";
        for ($i = 0; $i < count($arr); $i++){
            $str .= '   <tr>' . "\n";
            for ($j = 0; $j < $cnt; $j++){
                $str .='       <td>' . $arr[$i][$j] . '</td>' . "\n";
            }
            $str .= '   </tr>' . "\n";
        }
        $str .= '</table>' . "\n";
        if ($show){
            echo $str;
        }else{
            return $str;
        }
    }

    /**
     * Добавление/сохранение результата в БД
     * @param string $sqlType - Что сохраняем (squareSave - расчет, positionPriceSave - позиция, positionSquareSave - квадрат позиции) 
     * @param string $sqlAdd - добавить еще одну строку 
     * @param $end - true - расчет закончен
     */
    public function constructStringSql($sqlType, $sqlAdd='', $end=false, $tblName=''){
        if (!$tblName){
            $tblName = $this->tblName[$sqlType];
        }
        if (!$sqlAdd && !$end){
            return ;
        }
        
        if ((memory_get_usage() > $this->memoryLimit) || $end){
            if ($this->$sqlType){ // Если есть данные для добавления
                $this->db->InsertDataIntoTable($this->$sqlType, $tblName);
            }
            unset($this->$sqlType);
            if ($sqlAdd[0] === ','){
                $sqlAdd[0] = ' ';
            }
            $this->$sqlType = $sqlAdd;
        }else{
            $this->$sqlType .= $sqlAdd;
        }
    }
    
    /**
     * Суммирование двух аргументов с учетом диапазона [1..80]
     * @param $arg1
     * @param $arg2
     * @param $method (plus - простое сложение, invektor - инвектор числа, plMinus - плюс-минус, minus - разница между $arg1 и $arg2)
     * @return number
     */
    public function sum($arg1, $arg2=null, $method = 'plus'){
        switch ($method){
            case 'plus' :
                return (($arg1 + $arg2) > 80) ? ($arg1 + $arg2 - 80) : ($arg1 + $arg2);
                break;
            case 'minus' :
                return abs($arg1 - $arg2);
                break;
            case 'invektor' :
                return ($arg1 < 80) ? (80 - $arg1) : (80);
                break;
            case 'plMinus' :
                return (abs(80 - $arg1 + $arg2));
                break;
            default: 
                return null;
                break;
        }
    }
    
    /**
     * Увеличения каждого значения в массиве $arr на величину $prir
     * @param array $arr
     * @param number $prir
     */
    public function addPrirToSquare($arr, $prir){
        $this->tmpSql = '';
        // Двумерный массив
        if (count($arr[0]) > 1){
            for ($i = 0; $i < count($arr); $i++){
                for ($j = 0; $j < count($arr[$i]); $j++){
                    if (intval($arr[$i][$j]) > 0){ 
                        $arr[$i][$j] = $this->sum($arr[$i][$j] + $prir);
                        $this->tmpSql .= ', ' . $arr[$i][$j];
                    }
                }
            }
        }else{ // Одномерный массив
            for ($i = 0; $i < count($arr); $i++){
                $arr[$i] = $this->sum($arr[$i], $prir);
                $this->tmpSql .= ', ' . $arr[$i];
            }
        }
        return $arr;
    }

    /**
     * Изменение текущего квадрата
     * @param int $max - Сколько всего квадратов
     * @param int $plus - Приращение (+1 - вперед или -1 - назад)
     * @param string $what - какую переменную сессии меняем
     */
    public function changeNumberSquare($max = 80, $what='numSquare', $plus=1){
        if (!isset($_SESSION[$what])){
            $_SESSION[$what] = 0;
        }else{
            $numSq = intval($_SESSION[$what]);
            $numSq += intval($plus);
            $numSq = ($numSq < 0) ? ($max - 1) : $numSq;
            $numSq = ($numSq >= $max) ? 0 : $numSq;
            $_SESSION[$what] = $numSq;
        }
    }

    /**
     * Проверка существования значения $val в массиве $arr
     * @param number $val
     * @param array $arr
     * @return bool
     */
    private function isInArrVal($val, $arr){
        for($i = 0; $i < count($arr); $i++){
            if (intval($val) === intval($arr[$i])){
                return true;
            }
            if ($arr[$i] > $val){
                return false;
            }
        }
        return false;
    }

    /**
     * Возвращает все выбранные пользователем позиции
     * @return array
     */
    private function getPosition(){
        $sql = 'SELECT i from ' . TBLPREFIX . 'nowPosition';
        $res = $this->db->DoQuery($sql);
        $pos_ret = array();
        while($pos = mysqli_fetch_row($res)){
            $pos_tmp = explode(',', $pos[0]);
            $pos_ret[] = $pos_tmp[0];
        }
        return $pos_ret;
    }

    /**
     * Получение информации по всем выбранным пользователем позициям (сохраняет в $this->countSq)
     * @param int $typeArr - Тип массива (см. config/calc.descr.php по ключу typeSq)
     */
    public function GetInfoPosition($typeArr=0){
        $this->positionInfo = array();
        // Получаю все текущие позиции
        $_SESSION['position'] = $this->db->GetLastPosition($typeArr);
        $pos = $_SESSION['position'];
        $posId = array();
        $posId = explode(DELIMITER_STRING, $_SESSION['idPos']);
        $cnt = 0;
        unset($this->countSq);
        foreach($posId as $v){
            $cntNUmberInPosition = intval(substr_count($pos[$v], DELIMITER_STRING) + 1);
            foreach($this->masPrice[$cntNUmberInPosition] as $k1=>$v1){
                $sql = "SELECT count(id) FROM " . $this->tblName['positionSquareSave'] . " WHERE dateTir='$this->date' and numberCalculate=" . $_SESSION['chCalc'] . " and idPosition=$v and countSovp=$k1";
                $res = mysqli_fetch_row($this->db->DoQuery($sql));
                $this->countSq[$v][$cntNUmberInPosition][$k1] = $res[0];
                $cnt++;
            }
        }
        unset($this->countSq[0]);
    }
    
    /**
     * Пробегаемся по всем квадратам и выбираем позиции
     * @param int $startPosition - Начальная позиция внутри массива $arr, с которой нач-ся квадрат
     * @param array $arr -  Массив
     * @param $pos - Позиции
     */
    private function saveSqPosition($startPosition = 0, $arr, $pos){
        $this->tmpSql = '';
        $add = '(NULL, \'' . $this->date . '\', ' . $_SESSION['chCalc'];
        $idPos = explode(DELIMITER_STRING, $_SESSION['idPos']);
        // Позиции прогоняю
        for ($j = 0; $j < count($pos); $j++){

            // Количество чисел в позиции
            $cntNumInPos = substr_count($pos[$j], DELIMITER_STRING) + 1;
            $arrPos = explode(DELIMITER_STRING, $pos[$j]);
            // Сколько совпало из квадрата
            $cntSovpInSq = 0;
            for ($i = 0; $i < $cntNumInPos; $i++){
                // Если в квадрате есть ТАКАЯ ПОЗИЦИЯ
                if ($this->isInArrVal($arr[intval(intval($arrPos[$i]) + $startPosition)], $this->searchArr)){
                    $cntSovpInSq++;
                }
            }

            // Если позиция оказалась выигрышной - сохраняю ее
            if (isset($this->masPrice[$cntNumInPos][$cntSovpInSq])){
                if (!isset($this->countSq[$j])){
                    $this->countSq[$j] = array();
                }
                $this->countSq[$j][$cntNumInPos][$cntSovpInSq] = (!isset($this->countSq[$j][$cntNumInPos][$cntSovpInSq])) ? (1)
                                                                    : intval($this->countSq[$j][$cntNumInPos][$cntSovpInSq] + 1);
                $numSq = intval($this->countSq[$j][$cntNumInPos][$cntSovpInSq]);
                $comma = '';
                if (strlen($this->tmpSql)>0){
                    $comma = ',';
                }
                $this->tmpSql .= "$comma $add, $numSq, $cntSovpInSq, $idPos[$j], " . implode(', ', $arr) . ')';
            }
        }
    }

    /**
     * Построение статистики по выбранным позициям
     * @param int $typeArr - Тип массива (см. config/calc.descr.php по ключу typeSq)
     */
    public function statistics($typeArr=0){
        $this->tmpSql = '';

        $sql = 'SELECT * FROM ' . $this->tblName['position'] . ' WHERE typeArr = ' . $typeArr;

        $arrIdPos = explode(DELIMITER_STRING, $_SESSION['idPos']);
        $sqlAddPosition = ' and (idPosition = ' . $arrIdPos[0];
        for ($i = 1; $i < count($arrIdPos); $i++){
             $sqlAddPosition .= " or idPosition = " . $arrIdPos[$i];
        }
        
        $sqlAddPosition .= ');';
        
        $sql1 = 'SELECT * FROM ' . $this->tblName['positionSquareSave'] . " WHERE " .
            "dateTir='" . $this->date . "' and numberCalculate=" . $_SESSION['chCalc'] . " and numSq=1 $sqlAddPosition";
        
        $sql2 = 'SELECT * FROM ' . $this->tblName['squareSave'] . " WHERE dateTir='" . $this->date . "' 
                and numberCalculate=" . $_SESSION['chCalc'] . " and numberSquare=0 and numberAddition=0;";
        
        if (!mysqli_num_rows($this->db->DoQuery($sql)) ||
            !mysqli_num_rows($this->db->DoQuery($sql2))){// Расчет невозможен из-за отсутствия исх-х дынны
            return;
        }

        if (mysqli_num_rows($this->db->DoQuery($sql1)) > 1){ // Показываем квадраты
            return;
        }else{ // Рассчитываю
            $sel = '';
            $pos = $this->getPosition();
            for ($i = 1; $i < 81; $i++)
                $sel .= ', num' . $i;
            $sql = 'SELECT numberSquare, numberAddition' . $sel . ' FROM ' . $this->tblName['squareSave'] .
                ' WHERE dateTir=\'' . $this->date . '\' and numberCalculate=' . $_SESSION['chCalc'];
            // Получаю все квадраты
            $allSq = $this->db->DoQuery($sql);
            $cnt = 0;
            while($arr = mysqli_fetch_row($allSq)){
                $this->saveSqPosition(1, $arr, $pos);
                $comma = '';
                if ($this->tmpSql && $cnt){
                    $comma = ', ';
                }
                if ($this->tmpSql && !$cnt){ $cnt++;}
                $this->constructStringSql('positionSquareSave', $comma . $this->tmpSql);
            }
            $this->constructStringSql('positionSquareSave', $comma . $this->tmpSql, true);
            $_SESSION['positionInfo'] = implode(DELIMITER_STRING, $this->countSq);
        }
    }

    /**
     * Возвращает количество квадратов в 1 тираже расчета
     * @return int
     */
    public function retCountSquare(){
        $cntRot = ($this->maxRotate && $this->descript['isRotate']) ? $this->maxRotate : 1;
        $cntAdd = ($this->maxAddition && $this->descript['isAdditions']) ? $this->maxAddition : 1;
        return intval($cntRot * $cntAdd); 
    }

    /**
     * Построение пустого квадрата с выделением выбранной позиции
     * @param $arrInd - Индексы массива, задействованные в позиции
     * @param int $maxVal - Количество чисел в квадрате
     * @param int $cntCol - Количество чисел в строке
     */
    public function printNotFullArray($arrInd, $maxVal=80, $cntCol=9){
        echo '<table><tr>';
        $cnt = 0;
        $j = 0;
        // Предаю квадрату правильную форму (чтобы не было обрезанных строк)
        $maxVal = (intval($maxVal / $cntCol) * $cntCol < $maxVal) ? intval($maxVal / $cntCol) * $cntCol + $cntCol : $maxVal;
        for ($i = 0; $i < $maxVal; $i++){
            $cnt++;
            $class = '';
            
            if ($this->isInArrVal(intval($i+1), $arrInd)){
                $class = 'selectedTr1';
            }

            if ($cnt === 5 || $cnt === 4){
                $class .= ' redLeft';
            }

            if ($j === 4 || $j === 3){
                $class .= ' redBottom';
            }

            echo '<td class="' . $class . '">&nbsp;&nbsp;&nbsp;</td>';
            
            if ($cnt === $cntCol){
                echo '</tr>';
                $j++;
                $cnt = 0;
            }
        }
        echo '</tr></table>';
    }

    /**
     * В статистике получение выигрышного квадрата
     */
    public function GetResultTableArrOne(){
        $what = ' numberSquare, numberAddition, num1';
        for ($i = 2; $i <= 80; $i++){
            $what .= ', num' . $i;
        }
        
        $resAr = $this->db->getResultArrFromDb($this->date, $this->tblName['positionSquareSave'], $what);
        if ($resAr){
            $this->PrintTable($resAr, 1);
        }else{
            echo $this->lang['notFound'];
        }
    }

    /**
     * Вывод на экран таблицы статистики для выбранной позиции $curPos
     * @param number $curPos - id Выбранной позиции из табл. position
     */
    public function printResTableStat($curPos=null){
        if(is_null($curPos)){
            $curPos = intval($_SESSION['idPos_current']);
        }

        $numCountInPos = substr_count($_SESSION['position'][$curPos], DELIMITER_STRING) + 1;
        $this->pt->PrintTablResult($this->masPrice[$numCountInPos], $this->countSq[$curPos][$numCountInPos], $this->lang,$this->retCountSquare());
    }

    /**
     * Вывод результата - переопределяется в наследнике
     */
    public function PrintTable(){}
    
    /**
     * Сам рассчет (в каждом расчете - разный)
     */
    public function calculate(){}
}
