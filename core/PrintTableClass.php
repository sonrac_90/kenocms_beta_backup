<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 30.08.13
 * Time: 18:02
 * To change this template use File | Settings | File Templates.
 */

/**
 * Class KenoCMS_PrintTable
 * Строю различные таблицы для вывода рез-та
 */

class KenoCMS_PrintTable {

    /**
     * @var array Транслит
     */
    private $lang = array();

    /**
     * Конструктор
     * @param $lang Транслит
     */
    public function __construct ($lang){
        $this->lang = $lang;
    }

    /**
     * Вывожу строку рез-та
     * @param $res - Рез-т запроса из БД
     */
    public function PrintResultTable($res){
      
?>
        <div class=result><div><table id=result width=80%>
            <tbody>
            <?php
                $count = 0;
                while ($resultTir = mysqli_fetch_row($res)){
                    $count++;
                    $add = '';
                    if ($count % 5 === 0){
                        $add = " class=redBottom ";
                    }
                    echo "<tr $add>\n";
                    echo "<td class=redLeft>" . $resultTir[0] . "</td>\n";
                    echo "<td class=redLeft>" . $resultTir[1] . "." . $resultTir[2] . "." . $resultTir[3] . "</td>\n";
                    for ($i = 4; $i < count($resultTir); $i++){
                        $add = '';
                        if (intval($i - 3) % 5 === 0 && ($i + 1) < count($resultTir)){
                            $add = ' class=redLeft ';
                        }
                        echo "<td $add>" . $resultTir[$i] . "</td>\n";
                    }
                    echo "</tr>\n";
                }
            ?>
            </tbody>
        </table></div></div>
<?php
    }

    /**
     * В случае если в массиве $srch есть значение $val возвращает true, иначе - false
     * @param $val
     * @param $srch
     * @return bool
     */
    private function isArr($val, $srch){
        for ($i = 0; $i < count($srch); $i++){
            if (intval($val) === intval($srch[$i])){
                return true;
            }
            
            if (intval($val) < intval($srch[$i])){
                return false;
            }
        }
        return false;
    }

    /**
     * Выводит на экран квадрат 9х9 с центральной линией
     * @param null $arr
     * @param null $srch
     */
    public function PrintSquare9x9 ($arr=null, $srch=null, $startPosition=0){
        echo '<table class=resultSquare>';
        $cnt = $startPosition;
        if ($startPosition){
            echo $this->lang['itSquare'] . ' ' . $arr[0] . '-' . $arr[1];
            echo "<br/>";
        }
        
        for ($i = 0; $i < 9; $i++){
            echo '<tr>';
            for ($j = 0; $j < 9; $j++){
                $class = '';
                if (count($arr[0]) > 1){
                    $val = $arr[$i][$j];
                }else{
                    $val = $arr[$cnt];
                }
                
                if ($this->isArr($val, $srch)){
                    $class .= 'isActive';
                }
                
                if ($j === 4 || $j === 3){
                    $class .= ' redLeft';
                }
                
                if ($i === 4 || $i === 3){
                    $class .= ' redBottom';
                }
                
                echo "<td class='$class'>$val</td>";
                
                $cnt++;
            }
            echo '</tr>';
        }
        echo '</table>';
    }

    /**
     * Вывод на экран таблицы статистики выбранной позиции
     * @param array $price - Таблица со значениями выигрышей для выбранной позиции (см. config/config.php $masPrice)
     * @param array $arr - Массив с количеством совпадений по выбранной позиции
     * @param array $lang - Массив транслита
     * @param int $countSq - количество квадратов всего в расчете
     */
    public function PrintTablResult($price=array(), $arr=array(), $lang=array(), $countSq=80){
        echo '<table border=1 width=95% class=tablResultStat>';
        $cnt = 0;
        $all = 0;
        $cnt = 0;
        echo '<tr class=stat><td>' . $lang['statistic']['countNum'] . '</td>';
        echo '<td>' . $lang['statistic']['countSovp'] . '</td>';
        echo '<td>' . $lang['statistic']['sumOne'] . '</td>';
        echo '<td>' . $lang['statistic']['sumSum'] . '</td>';
        foreach($price as $k=>$v){
            $cnt++;
            $class = '';
            if (intval($_SESSION['idPosSumCurrent']) === $k){
                $class .= ' selectedTr1';
            }else if ($cnt % 2 !== 0){
                $class .= ' oneArr';
            }
            echo '<tr class="'. $class . '">';
            echo '<td>' . $k . '</td>';
            echo '<td>' . intval($arr[$k]) . '</td>';
            echo '<td>' . $v . '</td>';
            $priceComb = $v * $arr[$k];
            $all += $priceComb;
            echo '<td>' . $priceComb . '</td>'; 
            echo '</tr>';
        }
        $class = 'sumPrice';
        if (!isset($_SESSION['idPosSumCurrent']))
            $class = ' selectedTr1';
        echo '<tr class="' . $class . '"><td colspan="3" align=right>' . $lang['statistic']['sumSum'] . '</td><td>' . $all . '</td></tr>';
        echo '</table>';
        
        echo $lang['statistic']['stat'] . '<br/>';
        echo $lang['statistic']['sq'] . $countSq . '<br/>';
        echo $lang['statistic']['sumOneRate'] . SIZE_BET . '<br/>';
        echo $lang['statistic']['sumRate'] . intval($countSq * SIZE_BET) . '<br/>';
        echo $lang['statistic']['sumPrice'] . $all . '<br/>';
        echo $lang['statistic']['clearPrice'] . intval($all - $countSq * SIZE_BET) . '<br/>';
    }
}

?>
