<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 28.08.13
 * Time: 15:41
 * To change this template use File | Settings | File Templates.
 */

class KenoCMS_database {
    /**
     * @var string Имя БД
     */
    private $dbname = DBNAME;
    /**
     * @var string Хост
     */
    private $host = HOST;
    /**
     * @var string Пользователь
     */
    private $user = USER;
    /**
     * @var string Пароль
     */
    private $pass = PASS;
    /**
     * @var mysqli|null Коннектор к базе данных (ссылка)
     */
    public $connector = null;
    /**
     * @var array Транслит
     */
    private $lang = null;
    /**
     * @var KenoCMS_Error|null Ссылка на класс, который обрабатывает ошибки
     */
    private $err = null;

    /**
     * Конструктор
     * @param $lang - Транслит
     */
    public function __construct($lang){
        // Подключаюсь
        $this->connector = @mysqli_connect($this->host, $this->user, $this->pass, $this->dbname);
        $this->lang = $lang;
        $this->err = new KenoCMS_Error($lang);
        // Если была ошибка, вывешиваю баннер
        if (mysqli_connect_errno()){
            $debugInfo = debug_backtrace();
            $errmsg = $this->lang['connectText'] . $this->lang['textErr'] . '<div class=errAddMsg>' . mysqli_connect_error() . 
                '</div>' . $lang['inFile'] . $debugInfo[0]['file'] . '<br/>' . $lang['inStr'] . $debugInfo[0]['line'];
            $this->err->printError($errmsg, 'db');
            unset($errmsg);
        }
    }

    /**
     * Выполнить запрос
     * @param $sql - строка запроса
     * @return bool|mysqli_result|null - результат запроса
     */
    public function DoQuery($sql){
        $res = mysqli_query($this->connector, $sql);
        // В случае ошибки - вешаю баннер
        if (mysqli_errno($this->connector)){
            $debugInfo = debug_backtrace();
            $errmsg = $this->lang['queryText'] . '<div class=errAddMsg>' . $sql . '</div>' . $this->lang['textErr'] .
                '<div class="errAddMsg errAddMsrSystem">' . mysqli_error($this->connector) .
                '</div>' . $this->lang['inFile'] . $debugInfo[0]['file'] . '<br/>' . $this->lang['inStr'] .
                $debugInfo[0]['line'];
            $this->err->printError($errmsg, 'db');
            return null;
        }
        return $res;
    }

    /**
     * Получить результыт по $day и $month
     * @param numer $day - День
     * @param numer $month - Месяц
     * @return bool|mysqli_result|null
     */
    public function GetResultBall($day=null, $month=null){
        if (!$day || !$month){
            $day = $_SESSION['day'];
            $month = $_SESSION['month'];
        }
        
        if (!substr_count($_SESSION['resultTable'], TBLPREFIX)){
            $_SESSION['resultTable'] = TBLPREFIX . $_SESSION['resultTable'];
        }
        
        $sql = 'SELECT * FROM ' . $_SESSION['resultTable'] . " WHERE day=$day and month=$month";
        
        $res = $this->DoQuery($sql);
        return $res;
    }

    public function TruncateAllTable($isCreate=false){
        $db_sql = array(
            0 => array(
                'tblName' => TBLPREFIX.'calcresult_number80',
                'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'calcresult_number80 ( id int(15) NOT NULL AUTO_INCREMENT, dateTir varchar(10) NOT NULL, numberCalculate int(2) NOT NULL, numberSquare int(2) NOT NULL, numberAddition int(2) NOT NULL, num1 int(2) DEFAULT NULL, num2 int(2) DEFAULT NULL, num3 int(2) DEFAULT NULL, num4 int(2) DEFAULT NULL, num5 int(2) DEFAULT NULL, num6 int(2) DEFAULT NULL, num7 int(2) DEFAULT NULL, num8 int(2) DEFAULT NULL, num9 int(2) DEFAULT NULL, num10 int(2) DEFAULT NULL, num11 int(2) DEFAULT NULL, num12 int(2) DEFAULT NULL, num13 int(2) DEFAULT NULL, num14 int(2) DEFAULT NULL, num15 int(2) DEFAULT NULL, num16 int(2) DEFAULT NULL, num17 int(2) DEFAULT NULL, num18 int(2) DEFAULT NULL, num19 int(2) DEFAULT NULL, num20 int(2) DEFAULT NULL, num21 int(2) DEFAULT NULL, num22 int(2) DEFAULT NULL, num23 int(2) DEFAULT NULL, num24 int(2) DEFAULT NULL, num25 int(2) DEFAULT NULL, num26 int(2) DEFAULT NULL, num27 int(2) DEFAULT NULL, num28 int(2) DEFAULT NULL, num29 int(2) DEFAULT NULL, num30 int(2) DEFAULT NULL, num31 int(2) DEFAULT NULL, num32 int(2) DEFAULT NULL, num33 int(2) DEFAULT NULL, num34 int(2) DEFAULT NULL, num35 int(2) DEFAULT NULL, num36 int(2) DEFAULT NULL, num37 int(2) DEFAULT NULL, num38 int(2) DEFAULT NULL, num39 int(2) DEFAULT NULL, num40 int(2) DEFAULT NULL, num41 int(2) DEFAULT NULL, num42 int(2) DEFAULT NULL, num43 int(2) DEFAULT NULL, num44 int(2) DEFAULT NULL, num45 int(2) DEFAULT NULL, num46 int(2) DEFAULT NULL, num47 int(2) DEFAULT NULL, num48 int(2) DEFAULT NULL, num49 int(2) DEFAULT NULL, num50 int(2) DEFAULT NULL, num51 int(2) DEFAULT NULL, num52 int(2) DEFAULT NULL, num53 int(2) DEFAULT NULL, num54 int(2) DEFAULT NULL, num55 int(2) DEFAULT NULL, num56 int(2) DEFAULT NULL, num57 int(2) DEFAULT NULL, num58 int(2) DEFAULT NULL, num59 int(2) DEFAULT NULL, num60 int(2) DEFAULT NULL, num61 int(2) DEFAULT NULL, num62 int(2) DEFAULT NULL, num63 int(2) DEFAULT NULL, num64 int(2) DEFAULT NULL, num65 int(2) DEFAULT NULL, num66 int(2) DEFAULT NULL, num67 int(2) DEFAULT NULL, num68 int(2) DEFAULT NULL, num69 int(2) DEFAULT NULL, num70 int(2) DEFAULT NULL, num71 int(2) DEFAULT NULL, num72 int(2) DEFAULT NULL, num73 int(2) DEFAULT NULL, num74 int(2) DEFAULT NULL, num75 int(2) DEFAULT NULL, num76 int(2) DEFAULT NULL, num77 int(2) DEFAULT NULL, num78 int(2) DEFAULT NULL, num79 int(2) DEFAULT NULL, num80 int(2) DEFAULT NULL, PRIMARY KEY (id), KEY changesquare (numberCalculate,numberSquare,numberAddition), KEY changecalculate (dateTir,numberCalculate,numberSquare,numberAddition)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
            ),
            1 => array(
                'tblName' => TBLPREFIX.'nowPosition',
                'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'nowPosition ( id int(5) NOT NULL AUTO_INCREMENT, typearr int(2) NOT NULL, i text CHARACTER SET utf8 NOT NULL, PRIMARY KEY (id), KEY i (i(255))) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
            ),
            2 => array(
                'tblName' => TBLPREFIX.'position',
                'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'position ( id int(5) NOT NULL AUTO_INCREMENT, typeArr int(4) DEFAULT NULL, i text CHARACTER SET utf8 NOT NULL, PRIMARY KEY (id), KEY numberCalculate (typeArr,i(255))) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
            ),
            3 => array(
                'tblName' => TBLPREFIX.'positionResultSquare',
                'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'positionResultSquare ( id int(15) NOT NULL AUTO_INCREMENT, dateTir varchar(10) NOT NULL, numberCalculate int(2) NOT NULL, numSq int(4) NOT NULL, countSovp int(4) NOT NULL, idPosition int(8) NOT NULL, numberSquare int(2) NOT NULL, numberAddition int(2) NOT NULL, num1 int(2) DEFAULT NULL, num2 int(2) DEFAULT NULL, num3 int(2) DEFAULT NULL, num4 int(2) DEFAULT NULL, num5 int(2) DEFAULT NULL, num6 int(2) DEFAULT NULL, num7 int(2) DEFAULT NULL, num8 int(2) DEFAULT NULL, num9 int(2) DEFAULT NULL, num10 int(2) DEFAULT NULL, num11 int(2) DEFAULT NULL, num12 int(2) DEFAULT NULL, num13 int(2) DEFAULT NULL, num14 int(2) DEFAULT NULL, num15 int(2) DEFAULT NULL, num16 int(2) DEFAULT NULL, num17 int(2) DEFAULT NULL, num18 int(2) DEFAULT NULL, num19 int(2) DEFAULT NULL, num20 int(2) DEFAULT NULL, num21 int(2) DEFAULT NULL, num22 int(2) DEFAULT NULL, num23 int(2) DEFAULT NULL, num24 int(2) DEFAULT NULL, num25 int(2) DEFAULT NULL, num26 int(2) DEFAULT NULL, num27 int(2) DEFAULT NULL, num28 int(2) DEFAULT NULL, num29 int(2) DEFAULT NULL, num30 int(2) DEFAULT NULL, num31 int(2) DEFAULT NULL, num32 int(2) DEFAULT NULL, num33 int(2) DEFAULT NULL, num34 int(2) DEFAULT NULL, num35 int(2) DEFAULT NULL, num36 int(2) DEFAULT NULL, num37 int(2) DEFAULT NULL, num38 int(2) DEFAULT NULL, num39 int(2) DEFAULT NULL, num40 int(2) DEFAULT NULL, num41 int(2) DEFAULT NULL, num42 int(2) DEFAULT NULL, num43 int(2) DEFAULT NULL, num44 int(2) DEFAULT NULL, num45 int(2) DEFAULT NULL, num46 int(2) DEFAULT NULL, num47 int(2) DEFAULT NULL, num48 int(2) DEFAULT NULL, num49 int(2) DEFAULT NULL, num50 int(2) DEFAULT NULL, num51 int(2) DEFAULT NULL, num52 int(2) DEFAULT NULL, num53 int(2) DEFAULT NULL, num54 int(2) DEFAULT NULL, num55 int(2) DEFAULT NULL, num56 int(2) DEFAULT NULL, num57 int(2) DEFAULT NULL, num58 int(2) DEFAULT NULL, num59 int(2) DEFAULT NULL, num60 int(2) DEFAULT NULL, num61 int(2) DEFAULT NULL, num62 int(2) DEFAULT NULL, num63 int(2) DEFAULT NULL, num64 int(2) DEFAULT NULL, num65 int(2) DEFAULT NULL, num66 int(2) DEFAULT NULL, num67 int(2) DEFAULT NULL, num68 int(2) DEFAULT NULL, num69 int(2) DEFAULT NULL, num70 int(2) DEFAULT NULL, num71 int(2) DEFAULT NULL, num72 int(2) DEFAULT NULL, num73 int(2) DEFAULT NULL, num74 int(2) DEFAULT NULL, num75 int(2) DEFAULT NULL, num76 int(2) DEFAULT NULL, num77 int(2) DEFAULT NULL, num78 int(2) DEFAULT NULL, num79 int(2) DEFAULT NULL, num80 int(2) DEFAULT NULL, PRIMARY KEY (id), UNIQUE KEY selectSq (dateTir,numberCalculate,numSq,countSovp,idPosition), KEY countAllSq (dateTir,numberCalculate,idPosition), KEY countSq (dateTir,numberCalculate,countSovp,idPosition), KEY positionInfo (dateTir,numberCalculate,idPosition)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
            ),
            4 => array(
                'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'result_not_sort ( id int(11) NOT NULL AUTO_INCREMENT, day int(2) NOT NULL, month int(2) NOT NULL, year int(4) NOT NULL, b1 int(2) NOT NULL, b2 int(2) NOT NULL, b3 int(2) NOT NULL, b4 int(2) NOT NULL, b5 int(2) NOT NULL, b6 int(2) NOT NULL, b7 int(2) NOT NULL, b8 int(2) NOT NULL, b9 int(2) NOT NULL, b10 int(2) NOT NULL, b11 int(2) NOT NULL, b12 int(2) NOT NULL, b13 int(2) NOT NULL, b14 int(2) NOT NULL, b15 int(2) NOT NULL, b16 int(2) NOT NULL, b17 int(2) NOT NULL, b18 int(2) NOT NULL, b19 int(2) NOT NULL, b20 int(2) NOT NULL, PRIMARY KEY (id), UNIQUE KEY getResultTir (day,month,year), KEY resultTableGen (day,month)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
            ),
            5 => array(
                'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'result_sort ( id int(11) NOT NULL AUTO_INCREMENT, day int(2) NOT NULL, month int(2) NOT NULL, year int(4) NOT NULL, b1 int(2) NOT NULL, b2 int(2) NOT NULL, b3 int(2) NOT NULL, b4 int(2) NOT NULL, b5 int(2) NOT NULL, b6 int(2) NOT NULL, b7 int(2) NOT NULL, b8 int(2) NOT NULL, b9 int(2) NOT NULL, b10 int(2) NOT NULL, b11 int(2) NOT NULL, b12 int(2) NOT NULL, b13 int(2) NOT NULL, b14 int(2) NOT NULL, b15 int(2) NOT NULL, b16 int(2) NOT NULL, b17 int(2) NOT NULL, b18 int(2) NOT NULL, b19 int(2) NOT NULL, b20 int(2) NOT NULL, PRIMARY KEY (id), UNIQUE KEY getResultTir (day,month,year), KEY resultTableGen (day,month)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
            )
        );
        foreach($db_sql as $v){
            if (isset($v['tblName'])){
                $this->truncateTbl($v['tblName']);
            }
            
            if ($isCreate){
                $this->DoQuery($v['sqlCreate']);
            }
        }
    }
    
    /**
     * Получить шары из БД
     * @param number $day - День
     * @param number $month - Месяц
     * @param number $year - Год
     * @return array|null
     */
    public function GetResultArr($day=null, $month=null, $year=null, $table=null){
        if (!isset($day) && !isset($month) && !isset($year)){
            $day = $_SESSION['day'];
            $year = $_SESSION['year'];
            $month = $_SESSION['month'];
        }
        
        if (!isset($table)){
            $table = $_SESSION['resultTable'];
        }
        
        $what = "b1, b2, b3, b4, b5, b6, b7, b8, b9, b10, b11, b12, b13, b14, b15, b16, b17, b18, b19, b20";
        $where = "day = $day and month=$month and year=$year";
        $tbl = $table;

        return mysqli_fetch_row($this->SelectDataFromTable($what, $tbl, $where));
    }

    /**
     * Закрытие сенса с БД
     * @param null $conn - коннектор
     */
    public function close($conn=null){
        if ($conn){
            @mysqli_close($conn);
        }else{
            @mysqli_close($this->connector);
        }
    }

    /**
     * Вставка данных $data в таблицу $table
     * @param string $data
     * @param string $table
     * @return bool|mysqli_result|null
     */
    public function InsertDataIntoTable($data=null, $tblName=null){
        if (!substr_count($tblName, TBLPREFIX)){
            $tblName = TBLPREFIX . $tblName;
        }
        
        $sql = "INSERT INTO $tblName VALUES " . $data;
        return $this->DoQuery($sql);
    }

    /**
     * Выбор Значений $what из таблицы $table при условии $where
     * @param $what
     * @param $table
     * @param $where
     * @return bool|mysqli_result|null
     */
    public function SelectDataFromTable($what, $tblName, $where){
        if (!substr_count($tblName, TBLPREFIX)){
            $tblName = TBLPREFIX . $tblName;
        }
        $sql = "SELECT $what FROM $tblName WHERE $where";
        return $this->DoQuery($sql);
    }

    /**
     * Количество строк, которые возвратились в результате выполнения запроса
     * @param mysqli $res - Результат запроса
     * @return number
     */
    public function CountRow($res){
        return $res->num_rows;
    }

    /**
     * Проверка имеется ли тираж с датой date в таблице расчетов $_SESSION['chCalc'] $tblName
     * @param $tblName
     * @param $date
     * @return number - 1 в случае если есть и 0 - если нет
     */
    public function GetIsOrNotCalcDateInDb($tblName, $date, $cnt = true, $numSq=null){
        $add = "and numberSquare=0";
        $what = '*';
        if ($numSq){
            $add = "and numberSquare=" . $numSq[0] . " and numberAddition=" . $numSq[1];
            $what = $numSq[2];
        }
        $sql = "SELECT $what FROM " . $tblName . " WHERE dateTir='$date' and numberCalculate=" . $_SESSION['chCalc'] . " $add";
        $res = $this->DoQuery($sql);
        if ($cnt){
            return $this->CountRow($res);
        }
        
        return mysqli_fetch_row($res);
    }

    /**
     * @param $tbl
     * @return array|null
     */
    public function getMaxId($tblName){
        if (!substr_count($tblName, TBLPREFIX)){
            $tblName = TBLPREFIX . $tblName;
        }

        return mysqli_fetch_row($this->DoQuery('SELECT MAX(id) FROM ' . $tblName));
    }

    /**
     * Очистка таблицы $tblName
     * @param string $tblName
     * @return bool|mysqli_result|null
     */
    public function truncateTbl($tblName){
        if (!substr_count($tblName, TBLPREFIX)){
            $tblName = TBLPREFIX . $tblName;
        }
        $sql = 'TRUNCATE TABLE ' . $tblName;
        return $this->DoQuery($sql);
    }

    /**
     * Удаляет данные в таблице $tblName когда удовлетворяется условие $where
     * @param $tblName
     * @param $where
     * @return bool|mysqli_result|null
     */
    public function rmDataFromTable($tblName, $where){
        if (!substr_count($tblName, TBLPREFIX)){
            $tblName = TBLPREFIX . $tblName;
        }
        $sql = "DELETE FROM " . $tblName . " WHERE " .$where;
        return $this->DoQuery($sql);
    }

    /**
     * Сохранение выбранных пользователем позиций 
     * @param array $pos - Массив данных
     * @param array $posStr - Позиции
     * @param number $typeArr - Тип массива
     */
    public function GetPositionOrNot($pos=array(), $posStr, $typeArr){
        $n = false;
        $this->rmDataFromTable('nowPosition', ' typeArr='.$typeArr);
        foreach ($pos as $k=>$v){
            $sql = 'SELECT id FROM ' . TBLPREFIX . 'position WHERE i LIKE \'' . $posStr[$k] . '\' and typeArr=' . $typeArr;
            $res = mysqli_fetch_row($this->DoQuery($sql)); 
            if (!$res){
                $sql = "INSERT INTO " . TBLPREFIX . "position VALUES $v;";
                $this->DoQuery($sql);
                $sql = 'SELECT id FROM ' . TBLPREFIX . 'position WHERE i=\'' . $posStr[$k] . '\' and typeArr=' . $typeArr;
                $res = mysqli_fetch_row($this->DoQuery($sql));
            }
            
            if (!isset($_SESSION['idPos']) || $n){
                $comma = '';
                if (isset($_SESSION['idPos'])) $comma = DELIMITER_STRING;
                $_SESSION['idPos'] .= $comma . $res[0];
                $n = true;
            }
            $sql = 'INSERT INTO ' . TBLPREFIX . 'nowPosition VALUES (NULL, ' . $typeArr . ', \'' . $posStr[$k] . '\');';
            $this->DoQuery($sql);
        }
        $_SESSION['position'] = $posStr;
    }

    /**
     * Возвращает последние позиции, которые использовались в статистике в последний раз в квадратах типа $typeArr
     * @param $typeArr - Тип квадрата
     * @return string
     */
    public function GetLastPosition($typeArr=0){
        $sql = "SELECT i FROM " . TBLPREFIX . "nowPosition WHERE 1;";
        $res = $this->DoQuery($sql);
        $sql = array();
        $sqlId = array();
        $cnt = 0;
        while ($arr = mysqli_fetch_row($res)){
            $sql1 = "SELECT id FROM " . TBLPREFIX . "position WHERE i LIKE '" . $arr[0] . "' and typeArr=" . intval($typeArr);
            $res1 = $this->DoQuery($sql1);
            $arrRes = mysqli_fetch_row($res1);
            $sqlId[] = $arrRes[0];
            $sql[$arrRes[0]] = $arr[0];
            $cnt++;
        }
        $_SESSION['idPos'] = implode(DELIMITER_STRING, $sqlId);
        return $sql;
    }

    /**
     * Получение выигрышного квадрата из БД для статистики
     * @param $date - дата тиража
     * @param $tblName - имя таблицы, где хранятся результаты
     * @param string $what - что выбираем * - все
     * @return array|null
     */
    public function getResultArrFromDb($date, $tblName, $what='*'){
        if (!substr_count($tblName, TBLPREFIX)){
            $tblName = TBLPREFIX . $tblName;
        }
        $countSovp = '';
        if (isset($_SESSION['idPosSumCurrent'])){
            $countSovp = ' and countSovp = ' . intval($_SESSION['idPosSumCurrent']) . ' '; 
        }
        
        $limit = intval(intval($_SESSION['NumSqPosSovp']) - 1) . ',' . intval($_SESSION['NumSqPosSovp']) ;
        $where = "dateTir = '$date' and numberCalculate=" . $_SESSION['chCalc'] . " $countSovp and idPosition=" . 
                    $_SESSION['idPos_current'] . " limit $limit";
        $sql = 'SELECT ' . $what . ' FROM ' . $tblName . ' WHERE ' . $where;
        $res = $this->DoQuery($sql);
        return mysqli_fetch_row($res);
    }
    
    /**
     * Сборщик мусора (деструктор)
     */
    public function __destruct(){
    }
}

?>
