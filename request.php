<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 30.08.13
 * Time: 13:07
 * To change this template use File | Settings | File Templates.
 */

// Если есть конфиг для соединения с БД - работаем в нормальном режиме
require_once('config/config.php');
require_once('config/config.db.php');
if (!isset($_SESSION['visit'])){
    session_start();
}
if (isset($_POST['act'])){
    switch ($_POST['act']){
        // Set new date
        case 'setNewData':
            $resTable = array(TBLPREFIX . 'result_not_sort', TBLPREFIX . 'result_sort');
            $_SESSION['day'] = $_POST['day'];
            $_SESSION['month'] = $_POST['month'];
            $_SESSION['resultTable'] = $resTable[$_POST['resultTable']];
            require_once 'index.php';
            break;
        case 'goToCalcSet':
            foreach ($_POST as $key=>$value){
                if ($key !== 'act'){
                    $_SESSION[$key] = $value;
                }
                $_SESSION['numSquare'] = 0;
                $_SESSION['numAddition'] = 0;
            }
            unset($_SESSION['showResult']);
            break;
        case 'getVar':
            if (isset($_SESSION[$_POST['name']])){
                echo $_SESSION[$_POST['name']];
            }
            break;
        case 'setVar':
            if (isset($_SESSION[$_POST['name']])){
                $_SESSION[$_POST['name']] = $_POST['value'];
            }
            break;
        case 'deleteVarBack':
            foreach ($_SESSION as $k=>$v){
                if (!substr_count(BACKMAIN_SESSION_VARIABLE . $_POST['show'], $k)){
                    unset($_SESSION[$k]);
                    unset($k);
                    unset($v);
                }
            }
            if (!isset($_POST['show'])){
                $_SESSION['showResult'] = true;
            }
            
            if (isset($_POST['numberCalculate'])){
                $_SESSION['chCalc'] = $_POST['numberCalculate'];
                $_SESSION['baseCalc'] = $_POST['numberCalculate'];
            }
            break;
        case 'deleteVar':
            if (isset($_SESSION[$_POST['name']])){
                unset($_SESSION[$_POST['name']]);
                unset($_POST['name']);
            }
            break;
        case 'calculateStat':
            require_once 'config/calc.descr.php';
            require_once 'core/error.class.php'; // Класс ошибок
            $_SESSION['statistics'] = true;
            require_once 'core/database.class.php';
            require_once 'lang/ru/' . CHARSET . '/index.php';
            $db = new KenoCMS_database($langRec);

            unset($_SESSION['position']);
            unset($_SESSION['idPos']);
            unset($_SESSION['idPos_current']);
            unset($_SESSION['numIdPosCurrent']);
            unset($_SESSION['NumSqPosSovp']);
            if (isset($_POST['position'])){ // Устанавливаем новые позиции
                $positionPost = str_replace("'", '', $_POST['position']);
                $pos = json_decode($positionPost);
                $position = array();
                
                $dataBegin = '(NULL, ' . $descriptionCalculate[$_SESSION['chCalc']]['typeSq'];
                
                $strPos = array();
                foreach ($pos as $valuePos){
                    $str = '';
                    foreach($valuePos as $val){
                        if (!$str) $str .= "$val";
                        else $str .= DELIMITER_STRING . "$val";
                    }
                    
                    $position[] = $dataBegin . ', "' . $str . '")';
                    $strPos[] = $str;
                }
                require_once 'config/calc.descr.php';
                
                $db->GetPositionOrNot($position, $strPos, $descriptionCalculate[$_SESSION['chCalc']]['typeSq']);
                $_SESSION['statistics'] = true;
                $db->close();
                $db->__destruct();
            }else{ // Получаю последние позиции
                $_SESSION['position'] = $db->GetLastPosition($descriptionCalculate[$_SESSION['chCalc']]['typeSq']);
            }
            break;
        case 'changeCurrentSovpPosition':
            unset($_SESSION['NumSqPosSovp']);
            if (isset($_POST['curSovp'])){
                $_SESSION['idPosSumCurrent'] = $_POST['curSovp'];
            }else{
                unset($_SESSION['numIdPosCurrent']);
                unset($_SESSION['idPosSumCurrent']);
            }
            break;
        case 'checkIsSetPosition':
            require_once 'config/config.db.php';
            require_once 'core/error.class.php'; // Класс ошибок
            require_once 'core/database.class.php';
            require_once 'lang/' . LANG . '/' . CHARSET . '/index.php'; 
            require_once 'config/calc.descr.php';
            $db = new KenoCMS_database($langRec);
            $sql = "SELECT * FROM " . TBLPREFIX . "nowPosition WHERE typeArr=" . $descriptionCalculate[$_SESSION['chCalc']]['typeSq'];
            $res = $db->DoQuery($sql);
            if (!$res->num_rows){
                echo 'Нет позиций';
            }
            break;
        case 'changeSqMethod':
            $_SESSION['chCalc'] = intval($_SESSION['baseCalc']) + $_POST['newValue'];
            break;
        case 'getInfoCalculate':
            $numCalc = isset($_POST['numberCalculate']) ? intval($_POST['numberCalculate']) : intval($_SESSION['chCalc']);
            echo $numCalc;
            $numCalc = intval($numCalc/3);
            echo ' ' . $numCalc;
            $description = file_get_contents('config/help/' . $numCalc . '.html');
            echo $description;
            break;
    }
}
?>
