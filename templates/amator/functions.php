<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 30.08.13
 * Time: 13:17
 * To change this template use File | Settings | File Templates.
 */

/**
 * Creating Select Day and month
 * @return string - selectBox
 */
function CreateSelectElem(&$lang){
    $day = intval($_SESSION['day']);
    $month = intval($_SESSION['month']);
    
    $countDay = 31;
    
    if ($month === 2){
        $countDay = 28;
    }else if ($month === 8){
    }else{
        $month -= 1;
        if (($month % 2) === 0)
            $countDay = 30;
        $month += 1;
    }
    
    $html = "<select name=month class='newdesign'>\n";
    for ($i = 1; $i < 13; $i++){
        $add = '';
        if ($i === $month){
            $add = 'selected=selected';
        }
        $html .= "<option value=$i $add>$i - (" . $lang['month'][$i] . ")</option>\n";
    }
    $html .= "</select>\n";
    
    $html .= "<select name=day>\n";
    for ($i = 1; $i < $countDay; $i++){
        $add = '';
        if ($i === $day){
            $add = 'selected=selected';
        }
        $html .= "<option value=$i $add>$i</option>\n";
    }
    $html .= "</select>\n";
    
    $html .= '<select name="resultTable">\n';
    for ($i = 0; $i < 2; $i++){
        $sel = '';
        if (intval($_SESSION['resultTable']) === $i){
            $sel = " selected=selected ";
        }
        $html .= '<option value="' . $i . '" ' . $sel . ' >' . $lang['arrResult'][$i] . '</option>\n';
    }
    return $html;
}

function CreateTableResult(&$lang){
    $db = new KenoCMS_database($lang['db']);
    $calc = new KenoCMS_calculate($lang);
    $a = $calc->PrintResultTable($_SESSION['day'], $_SESSION['month']);
    $db->close();
    $db->__destruct();
}

/**
 * @param KenoCMS_calculate $calc
 */
function StatResult($calc){
    $calc->tblName['positionSquareSave'];
}
?>
