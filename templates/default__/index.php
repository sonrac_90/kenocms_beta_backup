<!doctype html>
<html>
<head>
    <meta charset="<?php echo CHARSET; ?>" />
    <title>
        <?php
            if (!isset($descriptionCalculate[$_SESSION['chCalc']]['titlePage']))
                echo $langRec['titleSite'];
            else
                echo $descriptionCalculate[$_SESSION['chCalc']]['titlePage'];
        ?>
    </title>
    <link rel="stylesheet" rev="stylesheet" href="templates/<?php echo TEMPLATES;?>/css/jquery-ui.css" />
    <link rel="stylesheet" rev="stylesheet" href="templates/<?php echo TEMPLATES;?>/css/main.css" />
    <script src="templates/<?php echo TEMPLATES;?>/js/jquery.js" ></script>
    <script src="templates/<?php echo TEMPLATES;?>/js/jquery-ui.min.js" ></script>
    <script src="templates/<?php echo TEMPLATES;?>/js/main.js" ></script>
    <?php
        if (isset($_REQUEST['choiseCalc'])){
            echo '<style>
                body{
                    overflow: auto;
                }
            </style>';
        }
    ?>
</head>
<body>
<?php
if (isset($_POST['choiseCalc'])){
    echo '<table class=showCalc>';
    echo '<tr><td>№</td>';
    foreach ($langRec['chCalc'] as $val){
        echo '<td>' . $val . '</td>';
    }
    echo '</tr>';
    foreach ($descriptionCalculate as $numCalcInConf=>$confCalc){
        if ($confCalc['parentCalc'] === 'not'){
            echo '<td>' . $numCalcInConf . '</td>';
            echo '<td>' . $confCalc['titlePage'] . '</td>';
            echo '<td>' . $confCalc['description'] . '</td>';
            echo '<td><div class=info ><a title=""><img src="templates/' . TEMPLATES . '/images/helpI.png" width=70px height=70px /></a></div>';
 
        }
        echo '</tr>';
    }
    echo '</table>';
}else{
?>
<div class=header align=center>
    <div class="infoChoise info">
        <a title="<?php echo $descriptionCalculate[$_SESSION['chCalc']]['description']; ?>"><img src='templates/<?php echo TEMPLATES;?>/images/helpI.png' /></a>
    </div>
    <div class="infoChoise infoChoiseLeft " id=choiseCalc>
        <a title="Выбор расчета"><img src='templates/<?php echo TEMPLATES;?>/images/List-Icon1.png' /></a>
        <?php
            echo $_SESSION['chCalc'];
            if (!isset($_SESSION['showResult'])){
                 echo '-' . $calculateVar->date;
            }
        ?>
    </div>

    <?php
require_once 'templates/' . TEMPLATES . '/functions.php';
require_once 'lang/ru/utf-8/index.php';
$showResultTable = (!isset($_POST['act']) || ($_POST['act'] === 'setNewData')) && $_SESSION['showResult'];
if ($showResultTable){
?>
    <form action="" method="POST">
        <input type="hidden" name=act value=setNewData />
<?php
    echo CreateSelectElem($langRec);
?>
    <input type="submit" value="<?php echo $langRec['changeData'];?>" class="select"/>
    </form>
<?php
    if ($descriptionCalculate[$_SESSION['chCalc']]['changeBuild']){ // Выбор метода построения квадратов
        echo '<select name=changeSqMethod>';
        for ($i = 0; $i < 3; $i++){
            $selected = '';
            if (intval(intval($_SESSION['chCalc']) - intval($_SESSION['baseCalc'])) === $i){
                $selected = 'selected';
            }
            echo "<option $selected     value=$i>" . $langRec['bSq'][$i] . "</option>";
        }
        echo '</select>';
    }
}else{
?>    <div class=backToMain>
        <a href='#' title="<?php echo $langRec['backMain'];?>"><?php echo $langRec['backMain'];?></a><br/>
        <?php
        if ($descriptionCalculate[$_SESSION['chCalc']]['isRotate'] || $descriptionCalculate[$_SESSION['chCalc']]['isAdditions']){
            if (isset($_POST['act']) && $_POST['act'] == 'changeSquareNumber' && (!isset($_POST['numPosition']))){
                if ($_POST['varChange']==='numberSquare'){
                    $_SESSION['numAddition'] = 0;
                    $max = $calculateVar->maxRotate;
                }else{
                    $max = $calculateVar->maxAddition;
                }
                $calculateVar->changeNumberSquare($max, $_POST['varChange'], $_POST['valuePlus']);
            }
        }
        if ($descriptionCalculate[$_SESSION['chCalc']]['isRotate'] && !isset($_SESSION['statistics'])){
            $_SESSION['numSquare'] = isset($_SESSION['numSquare']) ? $_SESSION['numSquare'] : 0;
            ?>
        <?php echo $langRec['sq'];?>№<button>&nbsp;&nbsp;+1&nbsp;&nbsp;</button>&nbsp;<?php echo $_SESSION['numSquare'];?>&nbsp;<button>&nbsp;&nbsp;-1&nbsp;&nbsp;</button>
        <?php }?><br/>
        
        <?php
        $additionIs = $descriptionCalculate[$_SESSION['chCalc']]['isAdditions'] && !isset($_SESSION['statistics']);
        if ($additionIs){
            $_SESSION['numAddition'] = isset($_SESSION['numAddition']) ? $_SESSION['numAddition'] : 0;
        ?>
        <?php }
        ?>
    </div>
<?php
}

?>
</div>
<?php
    if ($showResultTable){
        CreateTableResult($langRec);
    }else if (!isset($_SESSION['statistics'])){ // просмотр результатов (квадратов)
        echo '<div id=squareResult align=center>';
        echo '<div>';
        $calculateVar->PrintTable();
        ?>
        <input id=selectPosition type=button value="<?php echo $langRec['chooisePosition'];?>"/><br/>
        <input id=selectedPosition type=button value="<?php echo $langRec['calcChoiseAbovePosition'];?>" />

        <?php
        if($additionIs){
        // Блоки статистики
        ?>
        </div>
        <div id=prir >
            <button class=prir>&nbsp;&nbsp;+1&nbsp;&nbsp;</button>&nbsp;<?php echo $_SESSION['numAddition'];?>&nbsp;<button class=prir>&nbsp;&nbsp;-1&nbsp;&nbsp;</button>
        </div>
        
<?php
        }
        echo '</div>';
    }else{// статистика
        // Достаю необходимую информацию
        $positoinId = explode(DELIMITER_STRING, $_SESSION['idPos']);
        // Текущая позиция
        if (!isset($_SESSION['idPos_current'])){
            $_SESSION['idPos_current'] = $positoinId[0];
            $_SESSION['numIdPosCurrent'] = 0;
        }

        if ($_POST['act'] == 'changeSquareNumber' && $_POST['varChange'] == 'numPosition'){
            // Очищаю выбор совпадения позиции
            unset($_SESSION['idPosSumCurrent']);
            $x = intval($_SESSION['numIdPosCurrent']);
            $x += intval($_POST['valuePlus']);
            $maxPos = count($positoinId);
            if ($x >= $maxPos) $x = 0;
            if ($x < 0) $x = $maxPos - 1;
            $_SESSION['idPos_current'] = $positoinId[$x];
            $_SESSION['numIdPosCurrent'] = $x;
        }
        // Показываю таблицу статистики для позиции
        ?>
        <div class=statistic>
            <div class=statisticResultPos>
                <?php
                    echo $langRec['statistic']['titleTableResult'];
                    $calculateVar->GetInfoPosition($descriptionCalculate[$_SESSION['chCalc']]['typeSq']);
                    $calculateVar->printResTableStat();
                ?>
            </div>
            <div class=showPosition>
                <input type=button value='<?php echo $langRec['backSq']; ?>' id=backToSquare /><br/>
                <?php
                // Показываю стрелки выбора позиций
                if (count($positoinId) > 1){
                    ?>
                    <input type="button" class=positionChange value="&nbsp;&nbsp;+1&nbsp;&nbsp;" />
                    <?php echo $_SESSION['numIdPosCurrent'];?>
                    <input type="button" class=positionChange value="&nbsp;&nbsp;-1&nbsp;&nbsp;" /><br/>
                <?php
                }
                $calculateVar->printNotFullArray(explode(DELIMITER_STRING, $_SESSION['position'][$_SESSION['idPos_current']]));
                ?>
            </div>
            <div class=showPosition>
                <?php
                    $max = 0;
                    $cntPos = substr_count($_SESSION['position'][$_SESSION['idPos_current']], DELIMITER_STRING) + 1;
                    if (!isset($_SESSION['idPosSumCurrent'])){
                        foreach($calculateVar->countSq[$_SESSION['idPos_current']][$cntPos] as $v){
                            $max += intval($v);
                        }
                    }else{
                        $max = $calculateVar->countSq[$_SESSION['idPos_current']][$cntPos][$_SESSION['idPosSumCurrent']];
                    }
                    if (!isset($_SESSION['NumSqPosSovp'])){
                        $_SESSION['NumSqPosSovp'] = 1;
                    }
                     if ($_POST['act'] == 'changeSquareNumber' && $_POST['varChange'] == 'numSquarePosition'){
                         $x = $_SESSION['NumSqPosSovp'];
                         $x += intval($_POST['valuePlus']);
                         if ($x > $max) $x = 1;
                         if ($x < 1) $x = $max;
                         $_SESSION['NumSqPosSovp'] = $x;
                    }
                ?>
                    <input type="button" class=positionSquareChange value="&nbsp;&nbsp;+1&nbsp;&nbsp;" />
                    <?php echo intval($_SESSION['NumSqPosSovp']) . ' ' . $langRec['from'] . ' ' . $max;?>
                    <input type="button" class=positionSquareChange value="&nbsp;&nbsp;-1&nbsp;&nbsp;" /><br/>
                <?php
                    // Получаю необходимую информацию
                    $calculateVar->GetResultTableArrOne();
                ?>
            </div>
        </div>
<?php
        $calculateVar->db->close();
        $calculateVar->db->__destruct();
        $calculateVar->__destruct();
    }
}
?>
<div id=modal></div>
</body>
</html>
