/**
 * Created with JetBrains PhpStorm.
 * User: sonrac
 * Date: 15.09.13
 * Time: 23:50
 * To change this template use File | Settings | File Templates.
 */
$(document).ready(function (){
    $('input[type^=text]').each(function (){
        $(this).bind('click', function (event){
            event.preventDefault();
            $(this).val('');
        })
    });
    $('input[type^=password]').each(function (){
        $(this).bind('click', function (event){
            $(this).val('');
        })
    });
    
    $('input[type^=submit]').each(function (){
        if ($(this).val() === "Заполнить БД"){
            $(this).bind('click', function (){
                $('.error').each(function (){
                    $(this).empty();
                });
                var host = $('input[name^=host]').val();
                var user = $('input[name^=user]').val();
                var pass = $('input[name^=pass]').val();
                var namedb = $('input[name^=namedb]').val();
                var tblpref = $('input[name^=tblpref]').val();
                var OK = true;
                $.post(
                    'request.php',
                    {
                        act : 'checkConnect',
                        host: host,
                        user: user,
                        pass: pass
                    },
                    function (data){
                        if (data ){
                            OK = false;
                            $('body').append($('<div/>', {class: 'error'}).html(data));
                        }
                    }
                );
                var _self = this;
                if (OK){
                    $.post(
                        'request.php',
                        {
                            act: 'checkDB',
                            host: host,
                            user: user,
                            pass: pass,
                            namedb: namedb
                        },
                        function (data){
                            if (data){
                                $('body').append($('<div/>', {class: 'error'}).html(data));
                            }else{
                                $('body').append($('<div/>', {class: 'error'}).html('Ждите, пока заполнятся таблицы необходимой информацией!!!'));
                                $.post(
                                    'request.php',
                                    {
                                        'act' : 'appendDb',
                                        host: host,
                                        user: user,
                                        pass: pass,
                                        namedb: namedb,
                                        tblpref: tblpref
                                    },
                                    function (data){
                                        if (data && data.match(/OK/g).length < 8){
                                            $('body').append($('<div/>', {class: 'error'}).html(data));
                                        }else{
                                            $('body').append($('<div/>', {class: 'error'}).html(data));
                                            //alert('Таблицы созданы. Теперь необходимо перейти к следующему этапу: создание конфигурационного файла. Жмите "ОК"');
                                            if (confirm('Таблицы созданы. Теперь необходимо перейти к следующему этапу: создание конфигурационного файла. Жмите "ОК" или отмена для изменения данных'))
                                                $(_self).parent().submit();
                                        }
                                    }
                                );
                            }
                        }
                    );
                }
                return false;
            });
            
        }
    });
    
    $('input[type^=checkbox]').each(function (){
        $(this).bind('click', function (event){
            var check = this.checked;
            if (check){
                var d = document.getElementById('pass');
                d.type = 'text';
            }else{
                var d = document.getElementById('pass');
                d.type = 'password';
            }
        });
    });
});
