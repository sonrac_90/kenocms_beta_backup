<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 15.09.13
 * Time: 23:48
 * To change this template use File | Settings | File Templates.
 */

/**
 * Заполняет данные из csv файла $filename в таблицу $tblname
 * @param mysqli $mssql 
 * @param string $filename
 * @param string $tblname
 * @param string $delimiter - Разделитель данных
 */
function importCsv($mssql=null, $filename=null, $tblname=null, $delimiter=';'){
    $ok = false;
    $fileHandler = @fopen($filename, 'r');
    if ($fileHandler){
        echo mysqli_error($mssql);
        $ok = true;
        $countRow = 0;
        $sql = 'INSERT INTO ' . $tblname . ' VALUES ';
        $row = 0;
        while ($data = fgetcsv ($fileHandler, 1000, $delimiter))
        {
            $countRow++;
            $countCol = count($data);
            if ($countRow === 1)
                $sql .= '(' . $data[0];
            else
                $sql .= ', (' . $data[0];
            for ($i=1; $i < $countCol; $i++)
            {
                $sql .= ', ' . $data[$i];
            }
            $sql .= ')';
            if ($countRow > 4000){
                mysqli_query($mssql, $sql);
                $sql = 'INSERT INTO ' . $tblname . ' VALUES ';
                $countRow = 0;
            }
        }
        if ($countRow > 0)
            mysqli_query($mssql, $sql);
        
        if (mysqli_errno($mssql))
            $ok = false;
    }else{
        echo 'Невозможно открыть файл: ' . $filename . '<br/>';
    }
    return $ok;
}

if (isset($_POST['act'])){
    switch($_POST['act']){
        // Проверяю корректность введенных данных, а именно пароль, хост, пользователь и наличие модуля mysqli
        case 'checkConnect':
            $link = @mysql_connect($_POST['host'], $_POST['user'], $_POST['pass']);
            if ($link){
                mysql_close();
                try{
                    $id = @mysqli_connect($_POST['host'], $_POST['user'], $_POST['pass']);
                    if (!$id){
                        echo 'Невозможно подключиться по укзанным данным (хосту, пользователю и паролю). Проверьте их. <br/>Ответ mysqli' . mysqli_connect_errno();
                    }else
                        mysqli_close($id);
                }catch (Exception $excptn){
                        echo 'Не установлен модуль mysqli Для php. Установите его для работы!. Для debian и ubuntu: sudo aptitude install php5-mysqli\n' . $excptn;
                }
            }else{
                echo 'Невозможно подключиться по укзанным данным (хосту, пользователю и паролю). Проверьте их. <br/>Ответ mysql ' . mysql_error();
            }
            break;
        // Проверяю, создана ли БД и имеет ли пользователь к ней доступ
        case 'checkDB':
            try{
                $id = mysqli_connect($_POST['host'], $_POST['user'], $_POST['pass']);
                $connStatus = true;
                $msgMysql = '';
                if ($id){
                    $conn = @mysqli_select_db($id, $_POST['namedb']);
                    if (!$conn){
                        $sql = 'CREATE DATABASE IF NOT EXISTS ' . $_POST['namedb'] . ' DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;';
                        $res = mysqli_query($id, $sql);
                        if (mysqli_errno($id)){
                            $connStatus = false;
                            $msgMysql .= mysqli_error($id);
                        }else{
                            $conn = @mysqli_select_db($id, $_POST['namedb']);
                            $sql = 'CREATE TABLE test (id INT(5) NOT NULL AUTO_INCREMENT, text TEXT NULL, PRIMARY KEY(id));';
                            $res = @mysqli_query($id, $sql);
                            if (mysqli_connect_errno($id)){
                                $connStatus = false;
                                $msgMysql .= mysqli_error($id);
                            }else{
                                $conn = @mysqli_select_db($id, $_POST['namedb']);
                                $sql = 'DROP TABLE test;';
                                $res = mysqli_query($id, $sql);
                                if (mysqli_connect_errno($id)){
                                    $connStatus = false;
                                    $msgMysql .= mysqli_error($id);
                                }
                            }
                        }
                    }
                    mysqli_close($id);
                }else{
                    $connStatus = false;
                }
                
                if (!$connStatus){
                    echo 'Невозможно подключиться к указанной БД (или, если она существует, указанный пользователь не имеет к ней доступа), а у пользователя нет прав на создание базы данных';
                    echo '<br/> Ответ от Mysql: ' . $msgMysql;
                }
            }catch(Exception $excpt){
                
            }
            break;
        // Заполняю Базу данных необходимыми значениями
        case 'appendDb':
            $fname = array('result_not_sort.csv', 'result_sort.csv');
            // Создаю таблицы
            $sql = array();
            $db_sql = array(
                0 => array(
                    'tblName' => $_POST['tblpref'] . 'calcresult_number80',
                    'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'calcresult_number80 ( id int(15) NOT NULL AUTO_INCREMENT, dateTir varchar(10) NOT NULL, numberCalculate int(2) NOT NULL, numberSquare int(2) NOT NULL, numberAddition int(2) NOT NULL, num1 int(2) DEFAULT NULL, num2 int(2) DEFAULT NULL, num3 int(2) DEFAULT NULL, num4 int(2) DEFAULT NULL, num5 int(2) DEFAULT NULL, num6 int(2) DEFAULT NULL, num7 int(2) DEFAULT NULL, num8 int(2) DEFAULT NULL, num9 int(2) DEFAULT NULL, num10 int(2) DEFAULT NULL, num11 int(2) DEFAULT NULL, num12 int(2) DEFAULT NULL, num13 int(2) DEFAULT NULL, num14 int(2) DEFAULT NULL, num15 int(2) DEFAULT NULL, num16 int(2) DEFAULT NULL, num17 int(2) DEFAULT NULL, num18 int(2) DEFAULT NULL, num19 int(2) DEFAULT NULL, num20 int(2) DEFAULT NULL, num21 int(2) DEFAULT NULL, num22 int(2) DEFAULT NULL, num23 int(2) DEFAULT NULL, num24 int(2) DEFAULT NULL, num25 int(2) DEFAULT NULL, num26 int(2) DEFAULT NULL, num27 int(2) DEFAULT NULL, num28 int(2) DEFAULT NULL, num29 int(2) DEFAULT NULL, num30 int(2) DEFAULT NULL, num31 int(2) DEFAULT NULL, num32 int(2) DEFAULT NULL, num33 int(2) DEFAULT NULL, num34 int(2) DEFAULT NULL, num35 int(2) DEFAULT NULL, num36 int(2) DEFAULT NULL, num37 int(2) DEFAULT NULL, num38 int(2) DEFAULT NULL, num39 int(2) DEFAULT NULL, num40 int(2) DEFAULT NULL, num41 int(2) DEFAULT NULL, num42 int(2) DEFAULT NULL, num43 int(2) DEFAULT NULL, num44 int(2) DEFAULT NULL, num45 int(2) DEFAULT NULL, num46 int(2) DEFAULT NULL, num47 int(2) DEFAULT NULL, num48 int(2) DEFAULT NULL, num49 int(2) DEFAULT NULL, num50 int(2) DEFAULT NULL, num51 int(2) DEFAULT NULL, num52 int(2) DEFAULT NULL, num53 int(2) DEFAULT NULL, num54 int(2) DEFAULT NULL, num55 int(2) DEFAULT NULL, num56 int(2) DEFAULT NULL, num57 int(2) DEFAULT NULL, num58 int(2) DEFAULT NULL, num59 int(2) DEFAULT NULL, num60 int(2) DEFAULT NULL, num61 int(2) DEFAULT NULL, num62 int(2) DEFAULT NULL, num63 int(2) DEFAULT NULL, num64 int(2) DEFAULT NULL, num65 int(2) DEFAULT NULL, num66 int(2) DEFAULT NULL, num67 int(2) DEFAULT NULL, num68 int(2) DEFAULT NULL, num69 int(2) DEFAULT NULL, num70 int(2) DEFAULT NULL, num71 int(2) DEFAULT NULL, num72 int(2) DEFAULT NULL, num73 int(2) DEFAULT NULL, num74 int(2) DEFAULT NULL, num75 int(2) DEFAULT NULL, num76 int(2) DEFAULT NULL, num77 int(2) DEFAULT NULL, num78 int(2) DEFAULT NULL, num79 int(2) DEFAULT NULL, num80 int(2) DEFAULT NULL, PRIMARY KEY (id), KEY changesquare (numberCalculate,numberSquare,numberAddition), KEY changecalculate (dateTir,numberCalculate,numberSquare,numberAddition)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
                ),
                1 => array(
                    'tblName' => $_POST['tblpref'] . 'nowPosition',
                    'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'nowPosition ( id int(5) NOT NULL AUTO_INCREMENT, typearr int(2) NOT NULL, i text CHARACTER SET utf8 NOT NULL, PRIMARY KEY (id), KEY i (i(255))) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
                ),
                2 => array(
                    'tblName' => $_POST['tblpref'] . 'position',
                    'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'position ( id int(5) NOT NULL AUTO_INCREMENT, typeArr int(4) DEFAULT NULL, i text CHARACTER SET utf8 NOT NULL, PRIMARY KEY (id), KEY numberCalculate (typeArr,i(255))) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
                ),
                3 => array(
                    'tblName' => $_POST['tblpref'] . 'positionResultSquare',
                    'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'positionResultSquare ( id int(15) NOT NULL AUTO_INCREMENT, dateTir varchar(10) NOT NULL, numberCalculate int(2) NOT NULL, numSq int(4) NOT NULL, countSovp int(4) NOT NULL, idPosition int(8) NOT NULL, numberSquare int(2) NOT NULL, numberAddition int(2) NOT NULL, num1 int(2) DEFAULT NULL, num2 int(2) DEFAULT NULL, num3 int(2) DEFAULT NULL, num4 int(2) DEFAULT NULL, num5 int(2) DEFAULT NULL, num6 int(2) DEFAULT NULL, num7 int(2) DEFAULT NULL, num8 int(2) DEFAULT NULL, num9 int(2) DEFAULT NULL, num10 int(2) DEFAULT NULL, num11 int(2) DEFAULT NULL, num12 int(2) DEFAULT NULL, num13 int(2) DEFAULT NULL, num14 int(2) DEFAULT NULL, num15 int(2) DEFAULT NULL, num16 int(2) DEFAULT NULL, num17 int(2) DEFAULT NULL, num18 int(2) DEFAULT NULL, num19 int(2) DEFAULT NULL, num20 int(2) DEFAULT NULL, num21 int(2) DEFAULT NULL, num22 int(2) DEFAULT NULL, num23 int(2) DEFAULT NULL, num24 int(2) DEFAULT NULL, num25 int(2) DEFAULT NULL, num26 int(2) DEFAULT NULL, num27 int(2) DEFAULT NULL, num28 int(2) DEFAULT NULL, num29 int(2) DEFAULT NULL, num30 int(2) DEFAULT NULL, num31 int(2) DEFAULT NULL, num32 int(2) DEFAULT NULL, num33 int(2) DEFAULT NULL, num34 int(2) DEFAULT NULL, num35 int(2) DEFAULT NULL, num36 int(2) DEFAULT NULL, num37 int(2) DEFAULT NULL, num38 int(2) DEFAULT NULL, num39 int(2) DEFAULT NULL, num40 int(2) DEFAULT NULL, num41 int(2) DEFAULT NULL, num42 int(2) DEFAULT NULL, num43 int(2) DEFAULT NULL, num44 int(2) DEFAULT NULL, num45 int(2) DEFAULT NULL, num46 int(2) DEFAULT NULL, num47 int(2) DEFAULT NULL, num48 int(2) DEFAULT NULL, num49 int(2) DEFAULT NULL, num50 int(2) DEFAULT NULL, num51 int(2) DEFAULT NULL, num52 int(2) DEFAULT NULL, num53 int(2) DEFAULT NULL, num54 int(2) DEFAULT NULL, num55 int(2) DEFAULT NULL, num56 int(2) DEFAULT NULL, num57 int(2) DEFAULT NULL, num58 int(2) DEFAULT NULL, num59 int(2) DEFAULT NULL, num60 int(2) DEFAULT NULL, num61 int(2) DEFAULT NULL, num62 int(2) DEFAULT NULL, num63 int(2) DEFAULT NULL, num64 int(2) DEFAULT NULL, num65 int(2) DEFAULT NULL, num66 int(2) DEFAULT NULL, num67 int(2) DEFAULT NULL, num68 int(2) DEFAULT NULL, num69 int(2) DEFAULT NULL, num70 int(2) DEFAULT NULL, num71 int(2) DEFAULT NULL, num72 int(2) DEFAULT NULL, num73 int(2) DEFAULT NULL, num74 int(2) DEFAULT NULL, num75 int(2) DEFAULT NULL, num76 int(2) DEFAULT NULL, num77 int(2) DEFAULT NULL, num78 int(2) DEFAULT NULL, num79 int(2) DEFAULT NULL, num80 int(2) DEFAULT NULL, PRIMARY KEY (id), UNIQUE KEY selectSq (dateTir,numberCalculate,numSq,countSovp,idPosition), KEY countAllSq (dateTir,numberCalculate,idPosition), KEY countSq (dateTir,numberCalculate,countSovp,idPosition), KEY positionInfo (dateTir,numberCalculate,idPosition)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
                ),
                4 => array(
                    'tblName' => $_POST['tblpref'] . 'result_not_sort',
                    'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'result_not_sort ( id int(11) NOT NULL AUTO_INCREMENT, day int(2) NOT NULL, month int(2) NOT NULL, year int(4) NOT NULL, b1 int(2) NOT NULL, b2 int(2) NOT NULL, b3 int(2) NOT NULL, b4 int(2) NOT NULL, b5 int(2) NOT NULL, b6 int(2) NOT NULL, b7 int(2) NOT NULL, b8 int(2) NOT NULL, b9 int(2) NOT NULL, b10 int(2) NOT NULL, b11 int(2) NOT NULL, b12 int(2) NOT NULL, b13 int(2) NOT NULL, b14 int(2) NOT NULL, b15 int(2) NOT NULL, b16 int(2) NOT NULL, b17 int(2) NOT NULL, b18 int(2) NOT NULL, b19 int(2) NOT NULL, b20 int(2) NOT NULL, PRIMARY KEY (id), UNIQUE KEY getResultTir (day,month,year), KEY resultTableGen (day,month)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
                ),
                5 => array(
                    'tblName' => $_POST['tblpref'] . 'result_sort',
                    'sqlCreate' => 'CREATE TABLE IF NOT EXISTS ' . $_POST['tblpref'] . 'result_sort ( id int(11) NOT NULL AUTO_INCREMENT, day int(2) NOT NULL, month int(2) NOT NULL, year int(4) NOT NULL, b1 int(2) NOT NULL, b2 int(2) NOT NULL, b3 int(2) NOT NULL, b4 int(2) NOT NULL, b5 int(2) NOT NULL, b6 int(2) NOT NULL, b7 int(2) NOT NULL, b8 int(2) NOT NULL, b9 int(2) NOT NULL, b10 int(2) NOT NULL, b11 int(2) NOT NULL, b12 int(2) NOT NULL, b13 int(2) NOT NULL, b14 int(2) NOT NULL, b15 int(2) NOT NULL, b16 int(2) NOT NULL, b17 int(2) NOT NULL, b18 int(2) NOT NULL, b19 int(2) NOT NULL, b20 int(2) NOT NULL, PRIMARY KEY (id), UNIQUE KEY getResultTir (day,month,year), KEY resultTableGen (day,month)) ENGINE=MyISAM  DEFAULT CHARSET=utf8;'
                )
            );
            
            $pref = $_POST['tblpref'];

            // Подключаюсь к БД
            $id = mysqli_connect($_POST['host'], $_POST['user'], $_POST['pass'], $_POST['namedb']);
            foreach($db_sql as $v){
                echo 'Создается таблица ' .$v['tblName'] . '';
                $res = mysqli_query($id, $v['sqlCreate']);
                if (!$res){
                    echo mysqli_error($id);
                    echo ' not create<br/>';
                }else{
                    echo ' OK<br/>'; 
                }
            }
            $ok = false;
            // Заполняю данными таблицы результатов тиражей
            
            // Очищаю таблицы на всякий случай, во избежания дублей
            mysqli_query($id, 'TRUNCATE TABLE ' . $pref . 'result_sort');
            mysqli_query($id, 'TRUNCATE TABLE ' . $pref . 'result_not_sort');
            
            // Заполняю результаты в порядке выпадения
            echo 'Заполнение данными ... <br/> Результаты в порядке выпадения - ';
            if (!importCsv($id, $fname[0], $_POST['tblpref'] . 'result_not_sort')){
                echo 'Невозможно добавить данные из csv. <br/>' . mysqli_error($id);
            }else{
                echo 'OK<br/';
            }
            
            echo 'Результаты в порядке возрастания - ';
            // Заполняю результаты в порядке возрастания
            if (!importCsv($id, $fname[1], $_POST['tblpref'] . 'result_sort')){
                echo 'Невозможно добавить данные из csv. <br/>' . mysqli_error($id);
            }else{
                echo 'OK<br/';
            }
            $head = true;
            break;
    }
}

?>
