<!doctype>
<html>
<head>
    <meta charset="utf-8" />
</head>
<body>

<?php
session_start();
function NewSession(){
    $_SESSION['visit'] = 'comming';
    $_SESSION['day'] = 1;
    $_SESSION['month'] = 1;
    $_SESSION['year'] = 2002;
    $_SESSION['chCalc'] = 0;
    $_SESSION['baseCalc'] = 0;
    $_SESSION['showResult'] = true;
    $_SESSION['resultTable'] = 'result_not_sort';
}

NewSession();
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 14.09.13
 * Time: 21:24
 * To change this template use File | Settings | File Templates.
 */

$description = array(
);

echo '$descriptionCalculate = array(';

$methodPlus = 'plus';

// Тип построения базовой строки
$typeBuildStr = array(
    'horizontal' => array('horizontal', 'vertical'),
    'diapazon' => array('horizontal', 'vertical'),
    'one' => array(''),
);

$additionalsquare = '';

// Тип построения квадрата
$typeSquareBuild = array(0, 1, 2);

// Тип вращения
$typeRotate = array('', 'interval', 'rotate', 'intRotate');

// Помощь
// По каждому массиву, который прогоняется
$tmpText = ", таким образом образовывается последовательно 80 чисел. \\n2. Из базовой строки формируется начальный квадрат следующим образом:\\n";
$textTypeBuildStr = array(
    'horizontal' => array(
        'text' => "1. Рассчитывается Базовая Строка: каждая цифра исходной строки дополняется до повторения" . $tmpText,
        'descrTwo' => array(
            'horizontal' => " Полученный квадрат с числами последовательно пробегается по горизонтали и формируется квадрат 9х9\\n",
            'vertical' => " Полученный квадрат с числами последовательно пробегается по вертикали и формируется квадрат 9х9\\n"
        )
    ),
    'diapazon' => array(
        'text' => "1. Рассчитывается Базовая Строка: каждая цифра исходной строки дополняется до повторения следующим образом: если число - четное, то под ним дописываются только отсутствующие еще четные числа, если нечетные - то только отсутствующие нечетные." . $tmpText,
        'descrTwo' => array(
            'horizontal' => " Полученный квадрат с числами последовательно пробегается по горизонтали и формируется квадрат 9х9\\n", 
            'vertical' => " Полученный квадрат с числами последовательно пробегается по вертикали и формируется квадрат 9х9\\n"
        )
    ),
    'one' => array(
        'text' => "Базовой строкой является последовательность чисел от 1 до 80, выпавшие в тираже позиции не учитываются при построении Базовой Строки"
    )
);

$tmpText = "\\nПосле формирования Базовой Строки происходит вращение квадратов следующим образом";
$textTypeRotate = array(
    '' => '', 
    'interval' => $tmpText . "\\nЧисла заполняются через определенный интервал", 
    'rotate' => $tmpText . "\\nКвадрат вращается следующим образом: последняя цифра становится на 1 позицию, все остальные - сдвигаются вправо",
    'intRotate'  => $tmpText . "\\nСначала происходит вращение квадрата путем перемещения последней цифры на 1 позицию, а все остальные сдвигаются вправо, затем числа заполняются через определенный интервал"
);

$tmpText = "\\nКвадрат строится следующим образом:\\n";
$textTypeSquareBuild = array(
    'Строка в строку',
    'Спираль',
    'Диагональ'
);

$titlePage = array(
    'all' => '80 чисел. ', 
    'one' => '. Числа от 1 до 80.',
    'horizontal' => '(Горизонталь',
    'vertical' => '(Вертикаль',
    'horizontal1' => '-Горизонталь)',
    'vertical1' => '-Вертикаль)',
);

$titleSq = array('. Строка в строку', '. Спираль', '. Диагональ');
$cnt = 0;

foreach($typeBuildStr as $tBuildStrKey => $tBuildStr){
    foreach ($tBuildStr as $tBuildStrMethod){
        foreach($typeRotate as $tRotate){
            echo '<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;// ' . intval($cnt / 3) . ' Группа расчетов';
            foreach($typeSquareBuild as $tSquareBuild){
                $addition = 'true';
                if ($tRotate === 'rotate'){
                    $addition = 'false';
                }
                $descrNum = intval($cnt / 3);
                
                $description = $textTypeBuildStr[$tBuildStrKey]['text'] .
                               $textTypeBuildStr[$tBuildStrKey]['descrTwo'][$tBuildStrMethod];
                if ($tRotate){
                    $description .= $textTypeRotate[$tRotate];
                }

                $parent = (intval($cnt / 3)*3 === $cnt) ? 'not' : intval($cnt/3)*3;
                
                $description .= $tmpText . ": ";
                
                if ($parent){
                    $description .= implode(', ', $textTypeSquareBuild);
                }else{
                    $description .= $textTypeSquareBuild[$tSquareBuild];
                }
                
                $textTitlePage = $titlePage['all'] . $titlePage[$tBuildStrKey] . $titlePage[$tBuildStrMethod . '1'];
                
                $isRotate = ($tRotate) ? 'true' : 'false';
                echo '<br/>&nbsp;&nbsp;&nbsp;&nbsp;' . $cnt . ' => array (<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Краткое Описание';
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'description' => \"" . $description . '",';
                echo '<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Полное руководство (текст html или путь к странице, в которой хранится описание)';
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'help' => '" . $descrNum .".html',";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Есть ли приращения квадратов";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'isAdditions' => " . $addition . ",";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'typeRotate' => '" . $tRotate . "',";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Есть ли вращение квадратов";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'isRotate' => " . $isRotate . ",";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Тип квадратов";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'typeSq' => 0,";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Title страницы расчета";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'titlePage' => '" . $textTitlePage . $titleSq[$tSquareBuild] . "',";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Изменение метода построения квадратов";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'changeBuild' => true,";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Тип квадрата (0 - строка в строку, 1 - спираль, 2 - диагональ)";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'typeSquareBuild' => " . $tSquareBuild . ",";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Метод сложения";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'methodPlus' => 'plus',";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Метод построения базовой строки";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'typeBuildBaseStr' => '" . $tBuildStrKey . "',";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Метод построения базовой строки вторично (по горизонтали или по вертикали)";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'typeBuildBaseStrTwo' => '" . $tBuildStrMethod . "',";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;// Родительский расчет (используется тогда, когда расчеты в принципе одинаковы, но отличается лишь метод построения квадрата)";
                echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'parentCalc' => '" . $parent . "'";
                echo '<br/>&nbsp;&nbsp;&nbsp;&nbsp;),';
                $cnt++;
            }
        }
    }
}


echo ');';


// Помощь
// По каждому массиву, который прогоняется
$tmpText = ", таким образом образовывается последовательно 80 чисел (для примера взят 266 тираж (1.1.2002)):";
$textTypeBuildStr = array(
    'horizontal' => array(
        'text' => "1. Рассчитывается Базовая Строка: каждая цифра исходной строки дополняется до повторения" . $tmpText,
        'text1' => "<br/>2. Из базовой строки формируется начальный квадрат следующим образом:<br/>",
        'descrTwo' => array(
            'horizontal' => " Полученный квадрат с числами последовательно пробегается по горизонтали и формируется квадрат 9х9<br/>",
            'vertical' => " Полученный квадрат с числами последовательно пробегается по вертикали и формируется квадрат 9х9<br/>"
        )
    ),
    'diapazon' => array(
        'text' => "1. Рассчитывается Базовая Строка: каждая цифра исходной строки дополняется до повторения следующим образом: если число - четное, то под ним дописываются только отсутствующие еще четные числа, если нечетные - то только отсутствующие нечетные." . $tmpText,
        'text1' => "<br/>2. Из базовой строки формируется начальный квадрат следующим образом:<br/>",
        'descrTwo' => array(
            'horizontal' => " Полученный квадрат с числами последовательно пробегается по горизонтали и формируется квадрат 9х9<br/>",
            'vertical' => " Полученный квадрат с числами последовательно пробегается по вертикали и формируется квадрат 9х9<br/>"
        )
    ),
    'one' => array(
        'text' => "Базовой строкой является последовательность чисел от 1 до 80, выпавшие в тираже позиции не учитываются при построении Базовой Строки"
    )
);

$tmpText = "<br/>После формирования Базовой Строки происходит вращение квадратов следующим образом";
$textTypeRotate = array(
    '' => '',
    'interval' => $tmpText . "<br/>Числа заполняются через определенный интервал",
    'rotate' => $tmpText . "<br/>Квадрат вращается следующим образом: последняя цифра становится на 1 позицию, все остальные - сдвигаются вправо",
    'intRotate'  => $tmpText . "<br/>Сначала происходит вращение квадрата путем перемещения последней цифры на 1 позицию, а все остальные сдвигаются вправо, затем числа заполняются через определенный интервал"
);

$tmpText = "\nКвадрат строится следующим образом:\\n";
$textTypeSquareBuild = array(
    'Строка в строку',
    'Спираль',
    'Диагональ'
);


// Формирую подробное описание с примерами
require_once '../config/calc.descr.php';
$head = '<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="utf-8"/>
    <style >
        body {
            font-size: 20px;
        }
    </style>
</head>
<body>
';

require_once '../config/config.php';
require_once '../config/config.db.php';
require_once '../config/calc.descr.php';
require_once '../lang/ru/utf-8/index.php';
require_once '../core/database.class.php';
require_once '../core/error.class.php';
require_once '../core/PrintTableClass.php';
require_once '../core/Calculate/calculate.class.php';
require_once '../core/Calculate/Numb80Calc.php';

foreach ($descriptionCalculate as $k=>$v){
    if (intval($k/3)*3 === $k){
        $path = '../config/help/' . intval($k/3) . '.html';
        if (!@fopen('../config/help/' . intval($k/3) . '.html', "w")){
            $path = './' . intval($k/3) . '.html';
        }
        $fileHandler = fopen($path, "w");
        
        fwrite($fileHandler, $head);
        $baseStr = "1. Рассчитывается базовая строка следующим образом:<br/>\n" . $textTypeBuildStr[$v['typeBuildBaseStr']]['text'] . "\n";
        
        $calculateVar = new KenoCMS_Numb80Retry($langRec, $masPrice, $v);
        
        // Расчитываю 1 строку
        $a = $calculateVar->BuildFirstLine($v['typeBuildBaseStr'], 'horizontal', true);
        if ($v['typeBuildBaseStr'] !== 'one'){
          $baseStr .= $calculateVar->PrintArr($a, 20, false) . "\n" . $textTypeBuildStr[$v['typeBuildBaseStr']]['text1'] . "\n";
        }
        if (isset($textTypeBuildStr[$v['typeBuildBaseStr']]['descrTwo'][$v['typeBuildBaseStrTwo']])){
          $baseStr .= $textTypeBuildStr[$v['typeBuildBaseStr']]['descrTwo'][$v['typeBuildBaseStrTwo']] . '<br/>' . "\n\n";
          $a = $calculateVar->BuildFirstLine($v['typeBuildBaseStr'], $v['typeBuildBaseStrTwo'], true);
        }

        $baseStr .= '<div style="display: block; width:95%;">' . "\n";
        $b = array();
        $countArr = 0;
        foreach($typeSquareBuild as $keyType=>$valType){
            $baseStr .= "   <div style=\"display:block; float:left; margin-left:10px;\">\n   " . '2.1 Метод ' . $textTypeSquareBuild[$keyType] . ':<br/>' . "\n   ";
            $b[$countArr] = $calculateVar->buildArrayFromArr($calculateVar->retOneArrFromTwo($a, 20), 9, 9, $valType);
            $baseStr .= "   " . $calculateVar->PrintArr($b[$countArr], 9, false) . "\n   </div>\n   ";
            $countArr++;
        }
        $baseStr .="<div style='width: 100%; float:left; display: inline-block;'></div></div>\n<br/>";
        if ($v['isRotate']){
            
            if ($v['typeRotate']){
                $baseStr .= "<div style='width: 100%; display: inline-block;'><br/>\nПроисходит вращение квадратов следующим образом для примера возьму следующий квадрат после базового:<br/>\n". $textTypeRotate[$v['typeRotate']] . "\n<br/></div>";
                $baseStr .= '<div style="display: block; width:95%;"></div>' . "\n";
                $c = array();
                for ($i = 0; $i < count($b); $i++){
                    $baseStr .= "   <div style=\"display:block; float:left; margin-left:10px;\">\n   " . 'Для метода ' . $textTypeSquareBuild[$i] . ':<br/>' . "\n   ";
                    $m = array();
                    switch ($v['typeRotate']){
                        case 'interval' :
                            $m = $calculateVar->IntervalSquare($calculateVar->retOneArrFromTwo($a, 20), 1);
                            break;
                        case 'rotate' :
                            $m = $calculateVar->RotateSquare($calculateVar->retOneArrFromTwo($a, 20));
                            break;
                        case 'intRotate' :
                            $m = $calculateVar->IntervalSquare($calculateVar->RotateSquare($calculateVar->retOneArrFromTwo($a, 20)), 1);
                            break;
                    }
                    $c[$i] = $calculateVar->buildArrayFromArr($m, 9, 9, $i);
                    $baseStr .= "   " . $calculateVar->PrintArr($c[$i], 9, false) . "\n   </div>\n   ";
                }
                $baseStr .="</div>\n<br/>";
            }
        }else{
            $c = $b;
        }
        
        if ($v['isAdditions']){
          $baseStr .= "\n<div style='width: 100%; display: inline-block;'><br/>";
          $baseStr .= "   Далее происходит приращение к каждому квадрату от 1 до 79 (показано приращение к квадратам из предыдущего примера):<br/><br/>\n";
          for ($i = 0; $i < count($b); $i++){
              $baseStr .= "   <div style=\"display:block; float:left; margin-left:10px;\">\n   " . 'Для метода ' . $textTypeSquareBuild[$i] . ':<br/>' . "\n   ";
              $baseStr .= "   " . $calculateVar->PrintArr($calculateVar->addPrirToSquare($c[$i], 1), 9, false) . "\n   </div>\n   "; 
          }
          $baseStr .= "\n</div>";
        }
        fwrite($fileHandler, $baseStr);
        
        
        fwrite($fileHandler, "</body>\n</html>");
        fclose($fileHandler);
    }
}


?>
</body>
</html>
