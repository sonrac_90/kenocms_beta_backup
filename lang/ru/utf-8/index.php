<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Doniy Serhey 
 * Team: SoNRaC TeaM
 * Date: 28.08.13
 * Time: 17:55
 * To change this template use File | Settings | File Templates.
 */

// Транслит
$langRec = array(
    'month' => array (
        1 => 'Январь',
        2 => 'Февраль',
        3 => 'Март',
        4 => 'Апрель',
        5 => 'Май',
        6 => 'Июнь',
        7 => 'Июль',
        8 => 'Август',
        9 => 'Сентябрь',
        10 => 'Октябрь',
        11 => 'Ноябрь',
        12 => 'Декабрь'
    ),
    
    'titleSite' => 'Кено. Игра по системе',
    
    'tableRow' => array(
        '№ тир.',
        'Дата',
    ),

    'changeData' => 'Изменить дату',
    'arrResult' => array(
        'Не по нарастающей',
        'По нарастающей'
    ),
    
    'db' => array(
        'connect' => 'Ошибка при работе с БД',
        'connectText' => 'Проверьте конфигурацию. Невозможно подключиться',
        'textErr' => ' <br/> Текст ошибки mysql server: ',
        'queryText' => 'Невозможно выполнить запрос ',
        'inFile' => ' Вызвано в файле ',
        'inStr' => ' в строке '
    ),
    
    'backMain' => 'Вернуться на главную',
    'backSq' => 'Вернуться к квадратам',
    'chooisePosition' => 'Выбор позиции',
    'calcChoiseAbovePosition' => 'Рассчитать по ранее выбранной позиции',
    'sq' => 'Кв.',
    'add' => 'Прир.',
    'statistic' => array(
        'titleTableResult' => 'Статистика выпадения позиции:',
        'countNum' => 'Кол-во номеров',
        'countSovp' => 'Кол-во совпадений',
        'sumOne' => 'Выигрыш за совп.',
        'sumSum' => 'Общий выигрыш',
        'stat' => 'Общая статистика тиража:',
        'sq' => 'Количество квадратов: ',
        'sumRate' => 'Общая сумма ставок: ',
        'sumOneRate' => 'Одна ставка стоит: ',
        'sumPrice' => 'Выигрыш составил: ',
        'clearPrice' => 'Чистая прибыль: '
    ),
    'notFound' => 'Ничего не найдено в базе данных по данному запросу',
    'from' => 'из',
    'bSq' => array(
        1 => 'Спираль',
        2 => 'Диагональ',
        0 => 'Строка в строку'
    ),
    'itSquare' => 'Это квадрат',
    'tiraj' => 'тиража',
    'chCalc' => array(
        'Name' => 'Название',
        'shortDescription' => 'Короткое описание',
        'longDescription' => 'Полное описание. Для активации нажмите картинку'
    )
);

?>
